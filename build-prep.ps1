$utfEncoding = New-Object System.Text.UTF8Encoding($True)
$scriptPath = split-path -parent $MyInvocation.MyCommand.Definition
$gitCommit = ""
$gitBranch = ""

# Current version resides in version.txt
$fileText = [IO.File]::ReadAllText($scriptPath + "/version.txt").Trim().Split("_")

$versionText = $fileText[0]

$version = New-Object System.Version $versionText

# If we're building from CI, the BUILD_NUMBER env variable will be set
# So use it
if ($env:BUILD_NUMBER.Length -ne 0)
{
	$ma = $version.Major
	$mi = $version.Minor
	$pa = $version.Build # Stupid MS.  This is the revision/patch
	$build = [int]($env:BUILD_NUMBER)
	
	$version = New-Object System.Version $ma,$mi,$pa,$build
}

# Build our version attributes
$assemblyVersion = 'AssemblyVersion("' + $version + '")'
$assemblyFileVersion = 'AssemblyFileVersion("' + $version + '")'

# Read the current SolutionVersion.cs file
$solutionVersion = [IO.File]::ReadAllText($scriptPath + "/SolutionVersion.cs")

# Replace the relevant attributes
$solutionVersion = $SolutionVersion -replace "AssemblyVersion\(.+?\)", $assemblyVersion 
$solutionVersion = $SolutionVersion -replace "AssemblyFileVersion\(.+?\)", $assemblyFileVersion 

# Write it back
[IO.File]::WriteAllText($scriptPath + "/SolutionVersion.cs", $solutionVersion, $utfEncoding)

$commitsh = ""
$branch = ""

# Get the git commit hash, if it exists
if (Get-Command "git.exe" -ErrorAction SilentlyContinue)
{
	$gitCommit = git rev-parse --short head
	$gitBranch = git rev-parse --abbrev-ref HEAD
	
	$commitsh = "BuildCommit = `"" + $gitCommit + "`""
	$branch = "BuildBranch = `"" + $gitBranch + "`""
}

$buildInfo = [IO.File]::ReadAllText($scriptPath + "/Abacus.Common/BuildInfo.cs")
$buildInfo = $buildInfo -replace "BuildCommit = `"(.*?)`"", $commitsh
$buildInfo = $buildInfo -replace "BuildBranch = `"(.*?)`"", $branch

if ($fileText.Length -eq 2)
{
	$versionSuffix = $fileText[1]
	$buildInfo = $buildInfo -replace "BuildSuffix = `"(.*?)`"", "BuildSuffix = `"_$versionSuffix`""
} else {
	$buildInfo = $buildInfo -replace "BuildSuffix = `"(.*?)`"", "BuildSuffix = `"`""
}

[IO.File]::WriteAllText($scriptPath + "/Abacus.Common/BuildInfo.cs", $buildInfo, $utfEncoding)
