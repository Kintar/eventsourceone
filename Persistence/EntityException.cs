﻿using System;

namespace Persistence
{
    public class EntityException : Exception
    {
        public Type EntityType { get; protected set; }
        public Guid? Id { get; protected set; }

        public EntityException(Type entityType, Guid? id)
        {
            EntityType = entityType;
            Id = id;
        }

        public EntityException(string message, Type entityType, Guid? id = null) : base(message)
        {
            EntityType = entityType;
            Id = id;
        }
    }
}