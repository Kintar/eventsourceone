﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Persistence
{
    public class UnimplementedQueryException : Exception
    {
        public UnimplementedQueryException(IQueryParameters<IEntity> query) 
            : base(string.Format("No implementation found for query backed by parameters of type {0}", query.GetType()))
        {
        }

    }
}
