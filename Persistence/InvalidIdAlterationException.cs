﻿using System;

namespace Persistence
{
    public class InvalidIdAlterationException : EntityException
    {
        public InvalidIdAlterationException(IEntity e, Guid newId)
            : base(string.Format("Attempted to reset the ID of entity {0}#{1} to {2}", e.GetType().Name, e.Id, newId), e.GetType(), e.Id)
        {
        }
    }
}