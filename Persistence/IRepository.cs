﻿using System;
using System.Collections.Generic;

namespace Persistence
{
    public interface IRepository<T> where T : class, IEntity
    {
        void Save(T entity);
        
        T Get(Guid id);
        IEnumerable<T> Get(params Guid[] id);
        
        IEnumerable<Guid> QueryIds(IQueryParameters<T> queryParameters);
        IEnumerable<T> Query(IQueryParameters<T> queryParameters);
        
        void Delete(T entity);
    }
}
