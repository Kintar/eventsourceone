﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Persistence
{
    public class UniqueEntityViolation : EntityException
    {
        public UniqueEntityViolation(IEntity e) :
            base(
            string.Format("Cannot insert new {0} entity : This entity violates uniqueness constraints", e.GetType().Name),
            e.GetType(), e.Id)
        {

        }

        public UniqueEntityViolation(object e) :
            base(
            string.Format("Cannot insert new {0} entity : This entity violates uniqueness constraints", e.GetType().Name),
            e.GetType(), Guid.Empty)
        {

        }
    }
}
