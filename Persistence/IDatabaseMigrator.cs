﻿namespace Persistence
{
    public interface IDatabaseMigrator
    {
        void PerformMigration();
        int Priority { get; }
    }
}