﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Persistence
{
    public class DbMigratorService
    {
        private readonly IConnectionProvider connectionProvider;
        private readonly IEnumerable<IDatabaseMigrator> migrators;

        public DbMigratorService(IConnectionProvider connectionProvider, IEnumerable<IDatabaseMigrator> migrators)
        {
            this.connectionProvider = connectionProvider;
            this.migrators = migrators;
        }

        public void Migrate()
        {
            using (connectionProvider.GetConnection())
            {
                foreach (var migrator in migrators.OrderBy(x => x.Priority))
                {
                    migrator.PerformMigration();
                }
            }
        }
    }
}
