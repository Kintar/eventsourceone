﻿using System;
using System.Reflection;
using DbUp;
using DbUp.Helpers;

namespace Persistence
{
    public class AssemblyDatabaseMigrator : IDatabaseMigrator
    {
        private readonly IConnectionProvider connectionProvider;
        private readonly Assembly migrationAssembly;

        public AssemblyDatabaseMigrator(IConnectionProvider connectionProvider, Assembly migrationAssembly, int priority)
        {
            this.migrationAssembly = migrationAssembly;
            Priority = priority;
            this.connectionProvider = connectionProvider;
        }

        public void PerformMigration()
        {
            using (var conn = connectionProvider.GetConnection())
            {
                var upgrader = DeployChanges.To
                    .SqlDatabase(conn.ConnectionString)
                    .WithScriptsEmbeddedInAssembly(migrationAssembly,
                        s => s.ToLower().EndsWith(".sql") && !s.ToLower().Contains("repeating"))
                    .LogToConsole()
                    .Build();

                var result = upgrader.PerformUpgrade();
                if (!result.Successful)
                {
                    throw new Exception(
                        string.Format("Could not process database migrations in assembly {0}: {1} ", migrationAssembly.FullName, result.Error));
                }

                var repeatingUpgrades = DeployChanges.To
                    .SqlDatabase(conn.ConnectionString)
                    .WithScriptsEmbeddedInAssembly(migrationAssembly,
                        s => s.ToLower().EndsWith(".sql") && s.ToLower().Contains("repeating"))
                    .JournalTo(new NullJournal())
                    .LogToConsole()
                    .Build();

                result = repeatingUpgrades.PerformUpgrade();
                if (!result.Successful)
                {
                    throw new Exception(
                        string.Format("Could not perform repeating upgrades in assembly {0}: {1}", migrationAssembly.FullName, result.Error));
                }
            }
        }

        public int Priority { get; protected set; }
    }
}