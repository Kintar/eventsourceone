using System;

namespace Persistence
{
    public class ReadOnlyEntityException : EntityException
    {
        public ReadOnlyEntityException(IEntity e)
            : base(string.Format("Attempted to update read-only entity {0}#{1}", e.GetType().Name, e.Id), e.GetType(), e.Id)
        {
        }
    }
}