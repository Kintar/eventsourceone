﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Persistence.MSSQL
{
    public class SqlConnectionProvider : IConnectionProvider
    {
        protected readonly string ConnectionString;
        private CountedConnection connection;

        public SqlConnectionProvider(string connectionString)
        {
            this.ConnectionString = connectionString;
        }

        public IDbConnection GetConnection()
        {
            if (connection == null)
            {
                connection = new CountedConnection(CreateUnderlyingConnection());
                connection.LastEntryCleared += () => connection = null;
                connection.Open();
            }

            connection.Enter();
            return connection;
        }

        protected virtual IDbConnection CreateUnderlyingConnection()
        {
            return new SqlConnection(ConnectionString);
        }

        public void Dispose()
        {
            if (connection != null)
                connection.Dispose();
        }

        private class CountedConnection : IDbConnection
        {
            private IDbConnection connection;
            private int entryCounter = 0;
            private bool disposed;

            public void Enter()
            {
                if (disposed)
                    throw new ObjectDisposedException("Connection");

                entryCounter++;
            }

            public void Dispose()
            {
                entryCounter--;
                if (entryCounter == 0)
                {
                    connection.Dispose();
                    disposed = true;
                    if (LastEntryCleared != null)
                        LastEntryCleared();
                }
            }

            public delegate void LastEntryClearedDelegate();

            public event LastEntryClearedDelegate LastEntryCleared;

            public CountedConnection(IDbConnection connection)
            {
                this.connection = connection;
            }

            public IDbTransaction BeginTransaction()
            {
                return connection.BeginTransaction();
            }

            public IDbTransaction BeginTransaction(IsolationLevel il)
            {
                return connection.BeginTransaction(il);
            }

            public void Close()
            {
                connection.Close();
            }

            public void ChangeDatabase(string databaseName)
            {
                connection.ChangeDatabase(databaseName);
            }

            public IDbCommand CreateCommand()
            {
                return connection.CreateCommand();
            }

            public void Open()
            {
                connection.Open();
            }

            public string ConnectionString
            {
                get { return connection.ConnectionString; }
                set { connection.ConnectionString = value; }
            }

            public int ConnectionTimeout
            {
                get { return connection.ConnectionTimeout; }
            }

            public string Database
            {
                get { return connection.Database; }
            }

            public ConnectionState State
            {
                get { return connection.State; }
            }
        }
    }
}
