﻿using System;

namespace Persistence
{
    public class ConcurrentEntityUpdateException : EntityException
    {
        public ConcurrentEntityUpdateException(IEntity e)
            : base(string.Format("Entity {0}#{1} failed version check.  Current version is {2}", e.GetType().Name, e.Id, e.Version), e.GetType(), e.Id)
        {

        }

        public ConcurrentEntityUpdateException(Object o, object id)
            : base(string.Format("Could not update entity {0}#{1} : Entity has been modified by another transaction", o.GetType().Name, id),
            o.GetType())
        {
        }
    }

}