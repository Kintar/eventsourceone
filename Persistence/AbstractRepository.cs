﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using Dapper;

namespace Persistence
{
    public abstract class AbstractRepository<T> : IRepository<T> where T : class, IEntity
    {
        public abstract void Save(T entity);
        public abstract T Get(Guid id);
        public abstract IEnumerable<T> Get(params Guid[] id);
        public abstract void Delete(T entity);

        protected IConnectionProvider ConnectionProvider;

        protected AbstractRepository(IConnectionProvider connectionProvider)
        {
            ConnectionProvider = connectionProvider;
        }

        public IEnumerable<Guid> QueryIds(IQueryParameters<T> queryParameters)
        {
            var self = (dynamic) this;
            using (ConnectionProvider.GetConnection())
            {
                return self.RunQuery((dynamic) queryParameters);
            }
        }

        public IEnumerable<T> Query(IQueryParameters<T> queryParameters)
        {
            var self = (dynamic)this;
            using (ConnectionProvider.GetConnection())
            {
                return Get(self.RunQuery((dynamic) queryParameters).ToArray());
            }
        }

        protected IEnumerable<Guid> RunQuery(IQueryParameters<T> queryParameters)
        {
            throw new UnimplementedQueryException(queryParameters);
        }

        void IRepository<T>.Save(T entity)
        {
            try
            {
                Save(entity);
            }
            catch (SqlException ex)
            {
                if (ex.Message.Contains("UNIQUE KEY"))
                {
                    throw new UniqueEntityViolation(entity);
                }

                throw;
            }
        }

        protected void SaveOrUpdate(T entity, string insertSql, string updateSql)
        {
            using (var txn = new TransactionScope())
            using (var conn = ConnectionProvider.GetConnection())
            {
                var sql = updateSql;
                if (entity.Version == 0)
                {
                    sql = insertSql;
                    entity.Id = Guid.NewGuid();
                }

                var results = conn.Execute(sql, entity);
                if (results != 1)
                {
                    throw new ConcurrentEntityUpdateException(entity);
                }

                entity.Version++;
                txn.Complete();
            }
        }
    }
}
