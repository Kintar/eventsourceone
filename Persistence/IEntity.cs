﻿using System;

namespace Persistence
{
    public interface IEntity
    {
        Guid Id { get; set; }
        long Version { get; set; }
    }
}