﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using BankDomain.Contracts;
using BankDomain.Contracts.Events;
using BankDomain.Contracts.Values;
using BankDomain.Entities;
using BankDomain.Exceptions;
using Common.Domain;

namespace BankDomain
{
    public class AbacusBank : Aggregate, IAbacusBankService
    {
        private readonly IDictionary<Guid, LineItem> items;
        private readonly IDictionary<Guid, Bucket> buckets;
        private readonly IDictionary<Guid, ExternalLocation> locations;
        private readonly IDictionary<Guid, ContainerDefinition> containerDefinitions;
        private readonly IDictionary<Guid, Container> containers; 

        public AbacusBank(EventHandlerRegistry eventRegistry) : base(eventRegistry)
        {
            items = new Dictionary<Guid, LineItem>();
            buckets = new Dictionary<Guid, Bucket>();
            locations = new Dictionary<Guid, ExternalLocation>();
            containerDefinitions = new Dictionary<Guid, ContainerDefinition>();
            containers = new Dictionary<Guid, Container>();
        }

        public string Name { get; protected set; }
        public decimal OpeningBalance { get; protected set; }

        public IEnumerable<Bucket> Buckets
        {
            get { return buckets.Values; }
        }

        public IEnumerable<LineItem> LineItems
        {
            get { return items.Values; }
        }

        public void Create(string name)
        {
            if (Id != Guid.Empty)
            {
                throw new BankDomainException("Bank is already created!");
            }

            RaiseEvent(new BankCreated(Guid.NewGuid(), name));
        }

        protected void Apply(BankCreated evt)
        {
            Id = evt.BankId;
            Name = evt.Name;
        }

        public Guid CreateBucket(string name, BucketType type)
        {
            if (buckets.Values.Any(x => x.Type == type && x.Name.Equals(name, StringComparison.CurrentCultureIgnoreCase)))
            {
                throw new BucketExistsException(name, type);
            }

            var id = Guid.NewGuid();
            RaiseEvent(new BucketCreated(id, name, type));
            return id;
        }

        protected void Apply(BucketCreated evt)
        {
            var bucket = new Bucket();
            bucket.Apply(evt);
            buckets[bucket.Id] = bucket;
        }

        public Guid CreateLineItem(string name, Guid bucketId, MediaSign sign, decimal unit)
        {
            Bucket bucket;
            if (!buckets.TryGetValue(bucketId, out bucket))
            {
                throw new BucketNotFoundException(bucketId);
            }

            if (bucket.Items.Any(x => x.Name.Equals(name, StringComparison.CurrentCultureIgnoreCase)))
            {
                throw new LineItemExistsException(name, bucket.Name);
            }

            var newId = Guid.NewGuid();
            RaiseEvent(new LineItemCreated(newId, name, bucketId, sign, unit));
            return newId;
        }

        protected void Apply(LineItemCreated evt)
        {
            var bucket = buckets[evt.BucketId];
            var lineItem = new LineItem();
            lineItem.Apply(evt, bucket);
            items[lineItem.Id] = lineItem;
        }

        public void DisableLineItem(Guid lineId)
        {
            LineItem line;
            if (!items.TryGetValue(lineId, out line)) return;

            if (!line.Active) return;

            if (line.Value != 0)
                throw new LineItemNotEmptyException(line);

            RaiseEvent(new LineItemActiveChanged(lineId, false));
        }

        public void EnableLineItem(Guid lineId)
        {
            LineItem line;
            if (!items.TryGetValue(lineId, out line)) return;

            if (line.Active) return;

            RaiseEvent(new LineItemActiveChanged(lineId, true));
        }

        protected void Apply(LineItemActiveChanged evt)
        {
            items[evt.LineId].Apply(evt);
        }

        public void AdjustBalance(Guid accountabilityId, IEnumerable<LineItemAmount> lineChanges)
        {
            LineItem accountability;
            if (!items.TryGetValue(accountabilityId, out accountability))
            {
                throw new LineNotFoundException(accountabilityId, BucketType.Accountability);
            }

            if (accountability.Bucket.Type != BucketType.Accountability)
            {
                throw new InvalidLineItemException(string.Format("Line item #{0} is not an accountability item",
                    accountabilityId));
            }

            var changes = lineChanges.ToList();
            foreach (var change in changes)
            {
                LineItem item;
                if (!items.TryGetValue(change.LineId, out item))
                {
                    throw new LineNotFoundException(change.LineId, BucketType.Inventory);
                }

                if (!item.Active)
                {
                    throw new InvalidBankOperationException(
                        string.Format("Line #{0} [{1}] is disabled", item.Id, item.Name));
                }

                item.AssertAdjustmentOrThrow(change.Value);

                if (item.Bucket.Type != BucketType.Inventory)
                    throw new InvalidLineItemException(string.Format("Line item #{0} is not an inventory item", change.LineId));
            }

            RaiseEvent(new BalanceChanged(accountabilityId, changes));
        }

        protected void Apply(BalanceChanged evt)
        {
            var accountability = items[evt.AccountabilityLineId];
            var changes = evt.InventoryChanges.ToList();
            var inventories = items.Values.Where(x => changes.Any(c => c.LineId == x.Id)).ToList();

            accountability.Apply(evt);
            foreach (var inv in inventories)
            {
                inv.Apply(evt);
            }
        }

        public void MoveValue(Guid source, Guid dest, decimal amount)
        {
            var adjustments = new List<LineItemAmount>
            {
                new LineItemAmount(source, -amount),
                new LineItemAmount(dest, amount)
            };
            AdjustLines(adjustments);
        }

        public void MoveValue(IEnumerable<LineItemAmount> sourceLines, Guid destinationLine)
        {
            var adjustments = new List<LineItemAmount>(sourceLines);
            adjustments.Add(new LineItemAmount(destinationLine, adjustments.Sum(x => -x.Value)));
            AdjustLines(adjustments);
        }

        public void MoveValue(Guid sourceLine, IEnumerable<LineItemAmount> destinationLines)
        {
            var adjustments = new List<LineItemAmount>(destinationLines);
            adjustments.Add(new LineItemAmount(sourceLine, adjustments.Sum(x => -x.Value)));
            AdjustLines(adjustments);
        }

        private void AdjustLines(ICollection<LineItemAmount> adjustments)
        {
            var total = adjustments.Sum(x => x.Value);
            if (total != 0)
            {
                throw new TransactionBalanceException("The requested transaction would alter the balance of the bank");
            }

            if (adjustments.Any(x => !items.ContainsKey(x.LineId)))
            {
                throw new InvalidLineItemException("One or more requested line items were not found in the bank");
            }

            // Ensure this value movement is happeneing all on the same side,
            // either inventory or accountability
            var firstItem = items[adjustments.First().LineId];
            var bucketType = firstItem.Bucket.Type;
            if (adjustments.Select(adj => items[adj.LineId]).Any(item => item.Bucket.Type != bucketType))
            {
                throw new TransactionBalanceException(
                    "The requested transaction would alter the balance of the bank");
            }

            foreach (var change in adjustments)
            {
                LineItem item;
                if (!items.TryGetValue(change.LineId, out item))
                {
                    throw new InvalidLineItemException(change.LineId);
                }

                if (!item.Active)
                {
                    throw new InvalidBankOperationException(
                        string.Format("Line '{0}' [#{1}] is inactive", item.Name, item.Id));
                }

                item.AssertAdjustmentOrThrow(change.Value);
            }

            RaiseEvent(new ValueMoved(adjustments));
        }

        protected void Apply(ValueMoved evt)
        {
            foreach (var adjustment in evt.Adjustments)
            {
                var item = items[adjustment.LineId];
                item.Apply(evt);
            }
        }

        public void SettleBank()
        {
            var currentBalance = items.Values.Where(x => x.Bucket.Type == BucketType.Inventory)
                .Sum(x => x.Value);

            RaiseEvent(new BankSettled(DateTime.Now, currentBalance));
        }

        protected void Apply(BankSettled evt)
        {
            OpeningBalance = evt.ClosingBalance;
            var accountabilityItems = items.Values.Where(x => x.Bucket.Type == BucketType.Accountability).ToList();
            foreach (var item in accountabilityItems)
            {
                item.Apply(evt);
            }
        }

        public Guid CreateExternalLocation(string name, Guid sendLineId, Guid receiveLineId)
        {
            ValidateLineForLocationCreation(sendLineId);
            ValidateLineForLocationCreation(receiveLineId);

            name = name.Trim();
            if (locations.Values.Any(x => x.Name.Equals(name, StringComparison.CurrentCultureIgnoreCase)))
            {
                throw new DuplicateLocationException(name);
            }

            var locationId = Guid.NewGuid();
            RaiseEvent(new ExternalLocationCreated(locationId, name, sendLineId, receiveLineId));
            return locationId;
        }

        private void ValidateLineForLocationCreation(Guid sendLineId)
        {
            LineItem line;
            if (!items.TryGetValue(sendLineId, out line))
            {
                throw new InvalidLineItemException(string.Format("Could not find line item with id {0}",
                    sendLineId));
            }

            if (line.Bucket.Type != BucketType.Accountability)
            {
                throw new InvalidLineItemException(string.Format("Line item #{0} is not an accountability line",
                    sendLineId));
            }
        }

        protected void Apply(ExternalLocationCreated evt)
        {
            var sendLine = items[evt.SendLineId];
            var recvLine = items[evt.ReceiveLineId];

            var loc = new ExternalLocation();
            loc.Apply(evt, recvLine, sendLine);
            locations[loc.Id] = loc;
        }

        public void EnableExternalLocation(Guid locId)
        {
            ExternalLocation loc;
            if (!locations.TryGetValue(locId, out loc))
            {
                throw new InvalidLocationException(locId);
            }

            if (loc.IsActive) return;
            RaiseEvent(new ExternalLocationActiveChanged(true, locId));
        }

        public void DisableExternalLocation(Guid locId)
        {
            ExternalLocation loc;
            if (!locations.TryGetValue(locId, out loc))
            {
                throw new InvalidLocationException(locId);
            }

            if (!loc.IsActive) return;
            RaiseEvent(new ExternalLocationActiveChanged(false, locId));
        }

        protected void Apply(ExternalLocationActiveChanged evt)
        {
            locations[evt.LocationId].Apply(evt);
        }

        public void SendToLocation(Guid locationId, IEnumerable<LineItemAmount> lineChanges)
        {
            ExternalLocation loc;
            if (!locations.TryGetValue(locationId, out loc))
            {
                throw new InvalidLocationException(locationId);
            }

            if (!loc.IsActive)
            {
                throw new InvalidBankOperationException(
                    string.Format("Cannot send items: Location '{0}' is inactive", loc.Name));
            }

            var actualChanges = lineChanges.Select(x =>
                new LineItemAmount(x.LineId, -x.Value))
                .ToList();

            AdjustBalance(loc.SendAccountabilityLine.Id, actualChanges);
            RaiseEvent(new TransferredWithLocation(locationId, actualChanges.Sum(x => x.Value)));
        }

        public void ReceiveFromLocation(Guid locationId, IEnumerable<LineItemAmount> lineChanges)
        {
            ExternalLocation loc;
            if (!locations.TryGetValue(locationId, out loc))
            {
                throw new InvalidLocationException(locationId);
            }

            if (!loc.IsActive)
            {
                throw new InvalidBankOperationException(
                    string.Format("Cannot send items: Location '{0}' is inactive", loc.Name));
            }

            var changes = lineChanges.ToList();
            AdjustBalance(loc.ReceiveAccountabilityLine.Id, changes);
            RaiseEvent(new TransferredWithLocation(locationId, changes.Sum(x => x.Value)));
        }

        protected void Apply(TransferredWithLocation evt)
        {
            // Nothing to do
        }

        public Guid CreateContainerDefinition(string name, string type, IEnumerable<LineItemAmount> containerItems, Guid prepLineId, Guid outLineGuid)
        {
            if (containerDefinitions.Values.Any(x => x.Type.Equals(type, StringComparison.CurrentCultureIgnoreCase) &&
                x.Name.Equals(name, StringComparison.CurrentCultureIgnoreCase)))
            {
                throw new DuplicateContainerDefinitionException(name, type);
            }

            foreach (var id in new[] {prepLineId, outLineGuid})
            {
                LineItem line;

                if (!items.TryGetValue(id, out line))
                {
                    throw new InvalidLineItemException(id);
                }

                if (line.Bucket.Type != BucketType.Inventory)
                {
                    throw new InvalidLineItemException(
                        string.Format("Line '{0}', #{1}, is not an inventory item", line.Name, line.Id));
                }
            }

            var defId = Guid.NewGuid();
            RaiseEvent(new ContainerDefinitionCreated(defId, containerItems, name, type, prepLineId,
                outLineGuid));
            return defId;
        }

        protected void Apply(ContainerDefinitionCreated evt)
        {
            var def = new ContainerDefinition();
            def.Apply(evt, items[evt.PrepLineId], items[evt.OutLineId]);
            containerDefinitions[def.Id] = def;
        }

        public void DisableContainerDefinition(Guid defId)
        {
            ContainerDefinition def;
            if (!containerDefinitions.TryGetValue(defId, out def))
            {
                throw new InvalidContainerDefinitionException(defId);
            }

            if (!def.Active) return;
            RaiseEvent(new ContainerDefinitionActiveChanged(defId, false));
        }

        public void EnableContainerDefinition(Guid defId)
        {
            ContainerDefinition def;
            if (!containerDefinitions.TryGetValue(defId, out def))
            {
                throw new InvalidContainerDefinitionException(defId);
            }

            if (def.Active) return;
            RaiseEvent(new ContainerDefinitionActiveChanged(defId, true));
        }

        protected void Apply(ContainerDefinitionActiveChanged evt)
        {
            var def = containerDefinitions[evt.DefinitionId];
            def.Apply(evt);
        }

        public IEnumerable<Guid> CreateContainer(Guid containerDefinition, int count)
        {
            ContainerDefinition def;
            if (!containerDefinitions.TryGetValue(containerDefinition, out def))
            {
                throw new InvalidContainerDefinitionException(containerDefinition);
            }

            if (!def.Active)
            {
                throw new InvalidContainerDefinitionException(string.Format("Container definition {0} is inactive",
                    containerDefinition));
            }

            var media = def.Media.Select(
                x => 
                    new LineItemAmount(x.LineId, -x.Value * count))
                .ToList();

            var invalidMedia = media.Where(x => !items.ContainsKey(x.LineId)).ToList();
            if (invalidMedia.Any())
            {
                throw new InvalidLineItemException(string.Join(", ", invalidMedia.Select(y => y.LineId)));
            }

            var inactiveMedia = media.Where(x => !items[x.LineId].Active).ToList();
            if (inactiveMedia.Any())
            {
                throw new InvalidLineItemException(
                    string.Format("The following line items are inactive: {0}",
                        string.Join(", ", inactiveMedia.Select(x => 
                            string.Format("{0} [{1}]", items[x.LineId].Name, x.LineId)))));
            }

            var insufficientInventories = new List<LineItemAmount>();
            foreach (var item in media)
            {
                var line = items[item.LineId];
                if (!line.CanAdjustBy(item.Value))
                    insufficientInventories.Add(new LineItemAmount(line.Id, item.Value));
            }

            if (insufficientInventories.Any())
            {
                throw new InsufficientBalanceException(insufficientInventories);
            }

            var ids = new List<Guid>();
            var adjustments = def.Media.Select(x => new LineItemAmount(x.LineId, -x.Value)).ToList();
            adjustments.Add(new LineItemAmount(def.PrepLine.Id, def.Media.Sum(x => x.Value)));

            for (var i = 0; i < count; i++)
            {
                var containerId = Guid.NewGuid();
                RaiseEvent(new ValueMoved(adjustments));
                RaiseEvent(new ContainerCreated(containerId, containerDefinition));
                ids.Add(containerId);
            }
            return ids;
        }

        protected void Apply(ContainerCreated evt)
        {
            var container = new Container();
            container.Apply(evt, containerDefinitions[evt.DefinitionId]);
            containers[container.Id] = container;
        }

        public void ReclaimContainer(Guid containerDefinition, int count)
        {
            ContainerDefinition def;
            if (!containerDefinitions.TryGetValue(containerDefinition, out def))
            {
                throw new InvalidContainerDefinitionException(containerDefinition);
            }

            var ids = containers.Values.Where(x => x.Definition.Id == def.Id)
                .Take(count)
                .Select(x => x.Id)
                .ToList();

            if (ids.Count != count)
            {
                throw new InvalidBankOperationException(
                    string.Format("Cannot unmake {0} containers; only {1} in inventory"
                        , count, containers.Count(x => x.Value.Definition.Id == containerDefinition)));
            }

            var unmakeAmount = def.Media.Sum(x => -x.Value);

            if (!def.PrepLine.CanAdjustBy(unmakeAmount))
            {
                var mesage = string.Format(
                    "Insufficient value in container prep line '{1}' (#{0}) to un-make {2} containers.", def.PrepLine.Id, def.PrepLine.Name, count);
                throw new InvalidBankOperationException(mesage);
            }

            var adjustments = new List<LineItemAmount>(def.Media);
            adjustments.Add(new LineItemAmount(def.PrepLine.Id, unmakeAmount));
            foreach (var id in ids)
            {
                RaiseEvent(new ValueMoved(adjustments));
                RaiseEvent(new ContainerReclaimed(id, containerDefinition));
            }
        }

        protected void Apply(ContainerReclaimed evt)
        {
            containers.Remove(evt.ContainerId);
        }
    }
}
