﻿using System;

namespace BankDomain.Exceptions
{
    public class InvalidLocationException : BankDomainException
    {
        public InvalidLocationException(Guid locationId) : 
            base(string.Format("Location with id {0} does not exist", locationId))
        {
        }
    }
}