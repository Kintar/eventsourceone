﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BankDomain.Exceptions
{
    public class DuplicateLocationException : BankDomainException
    {
        public DuplicateLocationException(string locationName) : 
            base(string.Format("Location '{0}' already exists", locationName))
        {
        }
    }

    public class DuplicateContainerDefinitionException : BankDomainException
    {
        public DuplicateContainerDefinitionException(string containerName, string containerType) :
            base(string.Format("'{0}' container named '{1}' already exists", containerType, containerName))
        {
        }
    }
}
