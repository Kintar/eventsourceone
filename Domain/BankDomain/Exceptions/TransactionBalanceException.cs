﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BankDomain.Exceptions
{
    public class TransactionBalanceException : BankDomainException
    {
        public TransactionBalanceException(string message) : base(message)
        {
        }
    }
}
