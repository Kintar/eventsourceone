﻿namespace BankDomain.Exceptions
{
    public class LineItemExistsException : BankDomainException
    {
        public LineItemExistsException(string itemName, string bucketName) : base(string.Format("Line Item '{0}' already exists in bucket '{1}'", itemName, bucketName))
        {
            
        }
    }
}