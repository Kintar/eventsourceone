﻿using System;

namespace BankDomain.Exceptions
{
    public class BucketNotFoundException : BankDomainException
    {
        public BucketNotFoundException(Guid bucketId)
            : base(string.Format("Could not find bucket#{0}", bucketId))
        {
        }
    }
}