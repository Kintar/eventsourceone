﻿using System;
using BankDomain.Contracts.Values;

namespace BankDomain.Exceptions
{
    public class LineNotFoundException : BankDomainException
    {
        public LineNotFoundException(Guid lineId, BucketType type) : base(
            string.Format("Could not locate {1} line item #{0}", lineId, type.ToString().ToLower())
            )
        {
        }
    }
}