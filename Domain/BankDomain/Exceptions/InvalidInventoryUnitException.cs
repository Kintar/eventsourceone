using BankDomain.Entities;

namespace BankDomain.Exceptions
{
    public class InvalidInventoryUnitException : BankDomainException
    {
        public LineItem Item { get; protected set; }
        public decimal NewBalance { get; protected set; }

        public InvalidInventoryUnitException(LineItem item, decimal value) :
            base("The requested adjustment is not a whole number of inventory units")
        {
            Item = item;
            NewBalance = value;
        }
    }
}