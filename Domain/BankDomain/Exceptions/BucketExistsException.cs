﻿using BankDomain.Contracts.Values;

namespace BankDomain.Exceptions
{
    public class BucketExistsException : BankDomainException
    {
        public BucketExistsException(string bucketName, BucketType type)
            : base(string.Format("{0} bucket '{1}' already exists", type, bucketName))
        {

        }
    }
}