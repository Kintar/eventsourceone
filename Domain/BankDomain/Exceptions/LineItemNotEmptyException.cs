﻿using BankDomain.Entities;

namespace BankDomain.Exceptions
{
    public class LineItemNotEmptyException : InvalidBankOperationException
    {
        public LineItemNotEmptyException(LineItem item) : base(
            string.Format("Cannot disable line item '{0}' in bucket '{1}' : Line contains a current value of {2:C2}",
                item.Name, item.Bucket.Name, item.Value))
        {
        }
    }
}