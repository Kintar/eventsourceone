﻿using System;

namespace BankDomain.Exceptions
{
    public class InvalidLineItemException : BankDomainException
    {
        public InvalidLineItemException(string message) : base(message)
        {
        }

        public InvalidLineItemException(Guid id) : base(
            string.Format("Line item #{0} does not exist", id)
            )
        {
        }
    }
}