﻿using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using BankDomain.Contracts.Values;
using BankDomain.Entities;

namespace BankDomain.Exceptions
{
    public class InsufficientBalanceException : BankDomainException
    {
        private const string DefaultMessage = "Insufficient inventory for requested adjustment";
        public IEnumerable<LineItemAmount> Items { get; protected set; }

        public InsufficientBalanceException(LineItem item, decimal value) :
            base(DefaultMessage)
        {
            Items = new List<LineItemAmount>
            {
                new LineItemAmount(item.Id, value)
            };
        }

        public InsufficientBalanceException(IEnumerable<LineItemAmount> items) : base(DefaultMessage)
        {
            Items = new List<LineItemAmount>(items);
        }
    }
}