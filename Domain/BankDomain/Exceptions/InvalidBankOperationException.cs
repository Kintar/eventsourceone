﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BankDomain.Exceptions
{
    public class InvalidBankOperationException : BankDomainException
    {
        public InvalidBankOperationException(string message) : base(message)
        {
        }

        public InvalidBankOperationException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}
