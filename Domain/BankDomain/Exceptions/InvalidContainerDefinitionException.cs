﻿using System;

namespace BankDomain.Exceptions
{
    public class InvalidContainerDefinitionException : BankDomainException
    {
        public InvalidContainerDefinitionException(string message)
            : base(message)
        {
        }

        public InvalidContainerDefinitionException(Guid id)
            : base(
                string.Format("Container definition #{0} does not exist", id)
                )
        {
        }
    }
}