﻿using System;
using BankDomain.Contracts.Events;

namespace BankDomain.Entities
{
    public class Container
    {
        public Guid Id { get; protected set; }
        public ContainerDefinition Definition { get; protected set; }

        internal void Apply(ContainerCreated evt, ContainerDefinition definition)
        {
            Id = evt.ContainerId;
            Definition = definition;
        }
    }
}