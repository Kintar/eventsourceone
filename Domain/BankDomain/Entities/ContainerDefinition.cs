﻿using System;
using System.Collections.Generic;
using System.Linq;
using BankDomain.Contracts.Events;
using BankDomain.Contracts.Values;

namespace BankDomain.Entities
{
    public class ContainerDefinition
    {
        public Guid Id { get; protected set; }
        public IList<LineItemAmount> Media { get; protected set; }
        public string Name { get; protected set; }
        public string Type { get; protected set; }
        public LineItem PrepLine { get; protected set; }
        public LineItem OutLine { get; protected set; }
        public bool Active { get; protected set; }
        
        internal void Apply(ContainerDefinitionCreated evt, LineItem prepLine, LineItem outLine)
        {
            Id = evt.ContainerDefinitionId;
            Media = evt.Media.ToList();
            Name = evt.Name;
            Type = evt.Type;
            PrepLine = prepLine;
            OutLine = outLine;
            Active = true;
        }

        internal void Apply(ContainerDefinitionActiveChanged evt)
        {
            Active = evt.NewState;
        }
    }
}