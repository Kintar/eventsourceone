﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BankDomain.Contracts.Events;
using BankDomain.Contracts.Values;

namespace BankDomain.Entities
{
    public class Bucket
    {
        private readonly ICollection<LineItem> items = new LinkedList<LineItem>();

        public Guid Id { get; protected set; }
        public string Name { get; protected set; }
        public BucketType Type { get; protected set; }

        public IEnumerable<LineItem> Items
        {
            get { return items; }
        }

        internal void Apply(BucketCreated evt)
        {
            Id = evt.BucketId;
            Name = evt.Name;
            Type = evt.Type;
        }

        internal void AddItem(LineItem item)
        {
            items.Add(item);
        }
    }

}
