﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BankDomain.Contracts.Events;

namespace BankDomain.Entities
{
    public class ExternalLocation
    {
        public Guid Id { get; protected set; }
        public string Name { get; protected set; }
        public LineItem ReceiveAccountabilityLine { get; protected set; }
        public LineItem SendAccountabilityLine { get; protected set; }
        public bool IsActive { get; protected set; }

        internal void Apply(ExternalLocationCreated evt, LineItem recvLineItem, LineItem sendLineItem)
        {
            Id = evt.LocationId;
            Name = evt.Name;
            ReceiveAccountabilityLine = recvLineItem;
            SendAccountabilityLine = sendLineItem;
            IsActive = true;
        }

        internal void Apply(ExternalLocationActiveChanged evt)
        {
            IsActive = evt.NewState;
        }
    }
}
