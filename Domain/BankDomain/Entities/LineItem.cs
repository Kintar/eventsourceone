﻿using System;
using System.Linq;
using BankDomain.Contracts.Events;
using BankDomain.Contracts.Values;
using BankDomain.Exceptions;

namespace BankDomain.Entities
{
    public class LineItem
    {
        public Guid Id { get; protected set; }

        public string Name { get; protected set; }
        public MediaSign Sign { get; protected set; }
        public decimal Unit { get; protected set; }
        public decimal Value { get; protected set; }
        public Bucket Bucket { get; protected set; }
        public bool Active { get; protected set; }

        internal void Apply(LineItemCreated evt, Bucket bucket)
        {
            Id = evt.LineId;
            Name = evt.Name;
            Sign = evt.Sign;
            Unit = evt.Unit;
            Bucket = bucket;
            Active = true;
            bucket.AddItem(this);
        }

        internal void Apply(LineItemActiveChanged evt)
        {
            Active = evt.NewState;
        }

        internal void Apply(BalanceChanged evt)
        {
            var amount = Bucket.Type == BucketType.Accountability
                ? evt.InventoryChanges.Sum(x => x.Value)
                : evt.InventoryChanges.Single(x => x.LineId == Id).Value;

            AdjustValue(amount);
        }

        internal void Apply(ValueMoved evt)
        {
            var me = evt.Adjustments.SingleOrDefault(x => x.LineId == Id);
            if (me == null) return;

            AdjustValue(me.Value);
        }

        internal void Apply(BankSettled evt)
        {
            if (Bucket.Type == BucketType.Accountability)
            {
                Value = 0;
            }
        }

        private void AdjustValue(decimal amount)
        {
            Value += amount;
        }

        public bool IsSignValid(decimal newAmount)
        {
            if (newAmount == 0) return true;

            switch (Sign)
            {
                case MediaSign.Either:
                    return true;
                case MediaSign.Negative:
                    return newAmount < 0;
                case MediaSign.Positive:
                    return newAmount > 0;
                default:
                    throw new BankDomainException("Invalid MediaSign: " + Sign);
            }
        }

        public bool IsValidUnitAmount(decimal newAmount)
        {
            return newAmount % Unit == 0;
        }

        public bool CanAdjustBy(decimal adjustment)
        {
            var newAmount = Value + adjustment;
            return IsValidUnitAmount(newAmount) && IsSignValid(newAmount);
        }

        public void AssertAdjustmentOrThrow(decimal adjustment)
        {
            var newAmount = Value + adjustment;
            if (!IsSignValid(newAmount))
            {
                throw new InsufficientBalanceException(this, newAmount);
            }

            if (!IsValidUnitAmount(newAmount))
            {
                throw new InvalidInventoryUnitException(this, newAmount);
            }
        }
    }
}
