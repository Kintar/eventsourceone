﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Common.Domain
{
    public abstract class Aggregate : IAggregate
    {
        private readonly ICollection<object> uncommittedEvents = new LinkedList<object>();
        private readonly EventHandlerRegistry eventRegistry;

        protected Aggregate(EventHandlerRegistry eventRegistry)
        {
            this.eventRegistry = eventRegistry;
        }

        public Guid Id { get; protected set; }
        public int Version { get; protected set; }

        public void ClearUncomittedEvents()
        {
            uncommittedEvents.Clear();
        }

        public bool IsTransient
        {
            get { return Version == uncommittedEvents.Count; }
        }

        public IMemento GetSnapshot()
        {
            return null;
        }

        protected void RaiseEvent<TEvent>(TEvent evt)
            where TEvent : class
        {
            eventRegistry.Route(this, evt);
            uncommittedEvents.Add(evt);
            Version++;
        }

        public void Rebuild(IEnumerable<object> events)
        {
            if (Version != 0) throw new Exception("Aggregate is already constructed.  Cannot rebuild.");

            foreach (var evt in events)
            {
                eventRegistry.Route(this, evt);
                Version++;
            }
        }

        public IEnumerable<object> GetUncommittedEvents()
        {
            return uncommittedEvents;
        }
    }
}
