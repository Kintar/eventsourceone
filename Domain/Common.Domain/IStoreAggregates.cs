﻿using System;

namespace Common.Domain
{
    public interface IStoreAggregates
    {
        void Save<T>(T aggregate) where T : class, IAggregate;
        T Load<T>(Guid id) where T : class, IAggregate;
    }
}
