﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Common.Domain
{
    public class FunctionSourcedAggregateFactory : IAggregateFactory
    {
        private readonly IDictionary<Type, Func<IAggregate>> constructorFunctions;

        public FunctionSourcedAggregateFactory(IDictionary<Type, Func<IAggregate>> constructorFunctions)
        {
            this.constructorFunctions = constructorFunctions;
        }

        public T CreateInstance<T>() where T : class, IAggregate
        {
            Func<IAggregate> constructorFunc;
            if (!constructorFunctions.TryGetValue(typeof (T), out constructorFunc))
            {
                return null;
            }

            return (T)constructorFunc();
        }
    }
}
