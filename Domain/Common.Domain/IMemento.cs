﻿using System;

namespace Common.Domain
{
    public interface IMemento
    {
        Guid Id { get; set; }
        int Version { get; set; }
    }
}