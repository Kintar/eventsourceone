﻿using System;
using System.Collections.Generic;

namespace Common.Domain
{
    public interface IAggregate
    {
        Guid Id { get; }
        int Version { get; }

        /// <summary>
        /// Returns True if this aggregate has not yet been persisted
        /// </summary>
        bool IsTransient { get; }

        IEnumerable<object> GetUncommittedEvents();
        void ClearUncomittedEvents();

        IMemento GetSnapshot();

        void Rebuild(IEnumerable<object> events);
    }
}