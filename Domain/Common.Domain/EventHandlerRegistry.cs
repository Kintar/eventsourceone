﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Common.Domain
{
    public class EventHandlerRegistry
    {
        private readonly IDictionary<Type, IDictionary<Type, MethodInfo>> eventRoutes;

        public EventHandlerRegistry()
        {
            eventRoutes = new Dictionary<Type, IDictionary<Type, MethodInfo>>();
        }

        public void Route(IAggregate aggregate, object evt)
        {
            var method = GetMethodInfo(aggregate.GetType(), evt.GetType());
            try
            {
                method.Invoke(aggregate, new[] { evt });
            }
            catch (TargetInvocationException ex)
            {
                throw ex.InnerException;
            }
        }

        private MethodInfo GetMethodInfo(Type aggregate, Type evt)
        {
            IDictionary<Type, MethodInfo> routesForAggregate;
            if (!eventRoutes.TryGetValue(aggregate, out routesForAggregate))
            {
                routesForAggregate = FindEventRoutesForAggregate(aggregate);
                eventRoutes[aggregate] = routesForAggregate;
            }

            MethodInfo method;
            if (!routesForAggregate.TryGetValue(evt, out method))
            {
                throw new UnhandledEventException(string.Format("Aggregate {0} does not implement Apply for event {1}",
                    aggregate.FullName, evt.FullName));
            }

            return method;
        }

        private static IDictionary<Type, MethodInfo> FindEventRoutesForAggregate(Type aggregate)
        {
            var applyMethods = aggregate.GetMethods(BindingFlags.NonPublic | BindingFlags.Instance);

            applyMethods = applyMethods.Where(x => x.Name.Equals("Apply")).ToArray();

            var methodDict = new Dictionary<Type, MethodInfo>();
            foreach (var method in applyMethods)
            {
                var parameters = method.GetParameters();
                if (parameters.Length != 1)
                    continue;

                methodDict[parameters[0].ParameterType] = method;
            }

            return methodDict;
        }
    }
}
