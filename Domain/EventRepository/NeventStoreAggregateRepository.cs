﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common.Domain;
using NEventStore;

namespace EventRepository
{
    public class NeventStoreAggregateRepository : IStoreAggregates
    {
        private readonly IStoreEvents eventStore;
        private readonly IAggregateFactory aggregateFactory;

        public NeventStoreAggregateRepository(IStoreEvents eventStore, IAggregateFactory aggregateFactory)
        {
            this.eventStore = eventStore;
            this.aggregateFactory = aggregateFactory;
        }

        public void Save<T>(T aggregate) where T : class, IAggregate
        {
            IEventStream stream;
            if (aggregate.IsTransient)
            {
                stream = eventStore.CreateStream(aggregate.Id);
            }
            else
            {
                stream = eventStore.OpenStream(aggregate.Id, int.MaxValue);
            }

            using (stream)
            {
                foreach (var evt in aggregate.GetUncommittedEvents())
                {
                    stream.Add(new EventMessage
                    {
                        Body = evt
                    });
                }

                stream.CommitChanges(Guid.NewGuid());
            }

            aggregate.ClearUncomittedEvents();
        }

        public T Load<T>(Guid id) where T : class, IAggregate
        {
            using (var stream = eventStore.OpenStream(id, 1))
            {
                var aggregateInstance = aggregateFactory.CreateInstance<T>();
                var events = stream.CommittedEvents.Select(x => x.Body);
                aggregateInstance.Rebuild(events);
                return aggregateInstance;
            }
        }
    }
}
