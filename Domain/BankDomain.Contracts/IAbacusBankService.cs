using System;
using System.Collections.Generic;
using BankDomain.Contracts.Values;

namespace BankDomain.Contracts
{
    public interface IAbacusBankService
    {
        // Buckets
        Guid CreateBucket(string name, BucketType type);

        // Line Items
        Guid CreateLineItem(string name, Guid bucketId, MediaSign sign, decimal unit);
        void DisableLineItem(Guid lineId);
        void EnableLineItem(Guid lineId);

        // Locations
        Guid CreateExternalLocation(string name, Guid sendLineId, Guid receiveLineId);
        void EnableExternalLocation(Guid locId);
        void DisableExternalLocation(Guid locId);
        void SendToLocation(Guid locationId, IEnumerable<LineItemAmount> lineChanges);
        void ReceiveFromLocation(Guid locationId, IEnumerable<LineItemAmount> lineChanges);

        // Move money
        void AdjustBalance(Guid accountabilityId, IEnumerable<LineItemAmount> lineChanges);
        void MoveValue(Guid source, Guid dest, decimal amount);
        void MoveValue(IEnumerable<LineItemAmount> sourceLines, Guid destinationLine);
        void MoveValue(Guid sourceLine, IEnumerable<LineItemAmount> destinationLines);

        // Containers
        Guid CreateContainerDefinition(string name, string type, IEnumerable<LineItemAmount> containerItems, Guid prepLineId,
            Guid outLineGuid);

        void DisableContainerDefinition(Guid defId);
        void EnableContainerDefinition(Guid defId);

        IEnumerable<Guid> CreateContainer(Guid containerDefinition, int count);
        void ReclaimContainer(Guid containerDefinition, int count);

        // Settle
        void SettleBank();
    }
}