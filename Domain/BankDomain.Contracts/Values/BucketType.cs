﻿namespace BankDomain.Contracts.Values
{
    public enum BucketType
    {
        Inventory,
        Accountability
    }
}