﻿using System;

namespace BankDomain.Contracts.Values
{
    public class LineItemAmount
    {
        public Guid LineId { get; set; }
        public decimal Value { get; set; }

        public LineItemAmount(Guid lineId, decimal value)
        {
            LineId = lineId;
            Value = value;
        }

        public LineItemAmount()
        {
        }
    }
}
