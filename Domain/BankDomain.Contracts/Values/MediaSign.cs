﻿namespace BankDomain.Contracts.Values
{
    public enum MediaSign
    {
        Negative = -1,
        Either = 0,
        Positive = 1
    }
}