﻿using System;

namespace BankDomain.Contracts.Events
{
    public class ContainerDefinitionActiveChanged
    {
        public Guid DefinitionId { get; protected set; }
        public bool NewState { get; protected set; }

        protected ContainerDefinitionActiveChanged()
        {
        }

        public ContainerDefinitionActiveChanged(Guid definitionId, bool newState)
        {
            DefinitionId = definitionId;
            NewState = newState;
        }
    }
}