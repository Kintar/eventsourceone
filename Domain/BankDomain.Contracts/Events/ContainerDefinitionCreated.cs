﻿using System;
using System.Collections.Generic;
using BankDomain.Contracts.Values;

namespace BankDomain.Contracts.Events
{
    public class ContainerDefinitionCreated
    {
        protected ContainerDefinitionCreated()
        {
        }

        public ContainerDefinitionCreated(Guid containerDefinitionId,
            IEnumerable<LineItemAmount> media, string name, string type, Guid prepLineId, Guid outLineId)
           
        {
            ContainerDefinitionId = containerDefinitionId;
            Media = media;
            Name = name;
            Type = type;
            PrepLineId = prepLineId;
            OutLineId = outLineId;
        }

        public Guid ContainerDefinitionId { get; protected set; }
        public IEnumerable<LineItemAmount> Media { get; protected set; }
        public string Name { get; protected set; }
        public string Type { get; protected set; }
        public Guid PrepLineId { get; protected set; }
        public Guid OutLineId { get; protected set; }
    }
}