﻿using System;

namespace BankDomain.Contracts.Events
{
    public class ContainerCreated
    {
        protected ContainerCreated()
        {
        }

        public Guid ContainerId { get; protected set; }
        public Guid DefinitionId { get; protected set; }

        public ContainerCreated(Guid containerId, Guid definitionId)
        {
            ContainerId = containerId;
            DefinitionId = definitionId;
        }
    }
}