﻿using System;
using BankDomain.Contracts.Values;

namespace BankDomain.Contracts.Events
{
    public class LineItemCreated
    {
        public Guid LineId { get; protected set; }
        public string Name { get; protected set; }
        public Guid BucketId { get; protected set; }
        public MediaSign Sign { get; protected set; }
        public decimal Unit { get; protected set; }

        protected LineItemCreated()
        {
        }

        public LineItemCreated(Guid lineId, string name, Guid bucketId, MediaSign sign, decimal unit)
           
        {
            Name = name;
            BucketId = bucketId;
            Sign = sign;
            LineId = lineId;
            Unit = unit;
        }
    }
}