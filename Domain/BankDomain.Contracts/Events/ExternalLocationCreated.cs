﻿using System;

namespace BankDomain.Contracts.Events
{
    public class ExternalLocationCreated
    {
        public Guid LocationId { get; protected set; }
        public string Name { get; protected set; }
        public Guid SendLineId { get; protected set; }
        public Guid ReceiveLineId { get; protected set; }

        protected ExternalLocationCreated()
        {
        }

        public ExternalLocationCreated(Guid locationId, string name, Guid sendLineId, Guid receiveLineId)
        {
            LocationId = locationId;
            Name = name;
            SendLineId = sendLineId;
            ReceiveLineId = receiveLineId;
        }
    }
}
