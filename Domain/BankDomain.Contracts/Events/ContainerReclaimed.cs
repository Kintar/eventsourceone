﻿using System;

namespace BankDomain.Contracts.Events
{
    public class ContainerReclaimed
    {
        protected ContainerReclaimed()
        {
        }

        public Guid ContainerId { get; protected set; }
        public Guid DefinitionId { get; protected set; }

        public ContainerReclaimed(Guid containerId, Guid definitionId)
        {
            ContainerId = containerId;
            DefinitionId = definitionId;
        }
    }
}