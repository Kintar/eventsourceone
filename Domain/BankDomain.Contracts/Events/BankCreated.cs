﻿using System;

namespace BankDomain.Contracts.Events
{
    public class BankCreated
    {
        public Guid BankId { get; protected set; }
        public string Name { get; protected set; }

        protected BankCreated()
        {
        }

        public BankCreated(Guid bankId, string name)
        {
            BankId = bankId;
            Name = name;
        }
    }
}
