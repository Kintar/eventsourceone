﻿using System;

namespace BankDomain.Contracts.Events
{
    public class LineItemActiveChanged
    {
        public Guid LineId { get; protected set; }
        public bool NewState { get; protected set; }

        protected LineItemActiveChanged()
        {
        }

        public LineItemActiveChanged(Guid lineId, bool newState)
        {
            LineId = lineId;
            NewState = newState;
        }
    }
}
