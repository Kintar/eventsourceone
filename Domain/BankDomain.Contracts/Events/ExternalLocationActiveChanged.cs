﻿using System;

namespace BankDomain.Contracts.Events
{
    public class ExternalLocationActiveChanged
    {
        public bool NewState { get; protected set; }
        public Guid LocationId { get; protected set; }

        protected ExternalLocationActiveChanged()
        {
        }

        public ExternalLocationActiveChanged(bool newState, Guid locationId)
        {
            NewState = newState;
            LocationId = locationId;
        }
    }
}