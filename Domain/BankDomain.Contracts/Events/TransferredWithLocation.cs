﻿using System;

namespace BankDomain.Contracts.Events
{
    public class TransferredWithLocation
    {
        public Guid LocationId { get; protected set; }
        public decimal BankBalanceChange { get; protected set; }

        protected TransferredWithLocation()
        {
        }

        public TransferredWithLocation(Guid locationId, decimal bankBalanceChange)
        {
            LocationId = locationId;
            BankBalanceChange = bankBalanceChange;
        }
    }
}
