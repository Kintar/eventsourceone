﻿using System;

namespace BankDomain.Contracts.Events
{
    public class BankSettled
    {
        public DateTime SettledOn { get; protected set; }
        public decimal ClosingBalance { get; protected set; }

        protected BankSettled()
        {
        }

        public BankSettled(DateTime settledOn, decimal closingBalance)
        {
            SettledOn = settledOn;
            ClosingBalance = closingBalance;
        }
    }
}
