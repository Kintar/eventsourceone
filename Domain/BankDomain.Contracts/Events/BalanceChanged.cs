﻿using System;
using System.Collections.Generic;
using BankDomain.Contracts.Values;

namespace BankDomain.Contracts.Events
{
    public class BalanceChanged
    {
        private IEnumerable<LineItemAmount> inventoryChanges;
        public Guid AccountabilityLineId { get; protected set; }

        public IEnumerable<LineItemAmount> InventoryChanges
        {
            get { return inventoryChanges; }
            protected set { inventoryChanges = new List<LineItemAmount>(value); }
        }

        protected BalanceChanged()
        {
        }

        public BalanceChanged(Guid accountabilityLineId, IEnumerable<LineItemAmount> inventoryChanges)
        {
            AccountabilityLineId = accountabilityLineId;
            this.inventoryChanges = inventoryChanges;
        }
    }
}
