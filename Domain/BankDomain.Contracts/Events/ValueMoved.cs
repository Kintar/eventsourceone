﻿using System.Collections.Generic;
using BankDomain.Contracts.Values;

namespace BankDomain.Contracts.Events
{
    public class ValueMoved
    {
        private readonly ICollection<LineItemAmount> adjustments;

        public IEnumerable<LineItemAmount> Adjustments
        {
            get { return adjustments; }
        }

        protected ValueMoved()
        {
        }

        public ValueMoved(ICollection<LineItemAmount> adjustments)
        {
            this.adjustments = adjustments;
        }
    }
}