﻿using System;
using BankDomain.Contracts.Values;

namespace BankDomain.Contracts.Events
{
    public class BucketCreated
    {
        public Guid BucketId { get; protected set; }
        public string Name { get; protected set; }
        public BucketType Type { get; protected set; }

        protected BucketCreated()
        {
        }

        public BucketCreated(Guid bucketId, string name, BucketType type)
           
        {
            Name = name;
            Type = type;
            BucketId = bucketId;
        }
    }
}