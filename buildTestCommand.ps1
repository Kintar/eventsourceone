$scriptPath = split-path -parent $MyInvocation.MyCommand.Definition

Set-Location $scriptPath

$files = (gci -Exclude *RunTests*,*DataService* -Include *.Tests.dll -r Tests\ | ?{ $_.FullName -notmatch "\\obj\\?" } | Resolve-Path -Relative ) -join " "

$command = ".\tools\OpenCover.4.6.166\tools\OpenCover.Console.exe -register:user -returntargetcode -skipautoprops -output:`".\Tests\TestResults\coverage.xml`" -target:`".\tools\xunit.runner.console.2.1.0\tools\xunit.console.x86.exe`" -targetargs:`"$files -noshadow -html .\Tests\TestResults\UnitTests.html`"  -filter:`"-[*.Tests]* +[Abacus.*]Abacus.*`""

$command = $command + "`r`n"
$command = $command + "if %ERRORLEVEL% neq 0 exit %ERRORLEVEL%"
$command = $command + "`r`n"
$command = $command + ".\tools\ReportGenerator.2.3.3.0\tools\ReportGenerator.exe -reports:.\Tests\TestResults\coverage.xml -targetdir:.\Tests\TestResults\coverage"

[IO.File]::WriteAllText($scriptPath + "/executeTests.bat", $command)
