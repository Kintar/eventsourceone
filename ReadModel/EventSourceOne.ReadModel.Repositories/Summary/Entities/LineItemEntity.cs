﻿using Abacus.ReadModel.Contracts.Summary.Dto;
using Persistence;

namespace Abacus.ReadModel.Repositories.Summary.Entities
{
    public class LineItemEntity : LineItem, IEntity
    {
    }
}