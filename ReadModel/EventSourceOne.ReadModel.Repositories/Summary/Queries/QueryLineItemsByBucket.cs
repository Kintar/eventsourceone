﻿using System;
using Abacus.ReadModel.Repositories.Summary.Entities;
using Persistence;

namespace Abacus.ReadModel.Repositories.Summary.Queries
{
    public class QueryLineItemsByBucket : GameDateBasedQueryParameter, IQueryParameters<LineItemEntity>
    {
        public Guid BucketId { get; protected set; }

        public QueryLineItemsByBucket(DateTime gameDate, string shift, Guid bucketId) : base(gameDate, shift)
        {
            BucketId = bucketId;
        }
    }
}