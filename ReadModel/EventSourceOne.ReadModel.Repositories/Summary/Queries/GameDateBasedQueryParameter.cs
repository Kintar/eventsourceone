﻿using System;

namespace Abacus.ReadModel.Repositories.Summary.Queries
{
    public abstract class GameDateBasedQueryParameter
    {
        public DateTime GameDate { get; protected set; }
        public String Shift { get; protected set; }

        protected GameDateBasedQueryParameter(DateTime gameDate, string shift)
        {
            GameDate = gameDate;
            Shift = shift;
        }
    }
}