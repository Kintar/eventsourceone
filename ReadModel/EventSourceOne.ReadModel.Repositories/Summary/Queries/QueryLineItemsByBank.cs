﻿using System;
using Abacus.ReadModel.Repositories.Summary.Entities;
using Persistence;

namespace Abacus.ReadModel.Repositories.Summary.Queries
{
    public class QueryLineItemsByBank : GameDateBasedQueryParameter, IQueryParameters<LineItemEntity>
    {
        public Guid BankId { get; protected set; }

        public QueryLineItemsByBank(DateTime gameDate, string shift, Guid bankId) : base(gameDate, shift)
        {
            BankId = bankId;
        }
    }
}