﻿using System;
using Abacus.ReadModel.Repositories.Summary.Entities;
using Persistence;

namespace Abacus.ReadModel.Repositories.Summary.Queries
{
    public class QueryBucketsByBank : GameDateBasedQueryParameter, IQueryParameters<BucketEntity>
    {
        public QueryBucketsByBank(DateTime gameDate, string shift, Guid bankId) : base(gameDate, shift)
        {
            BankId = bankId;
        }

        public Guid BankId { get; protected set; }
    }
}