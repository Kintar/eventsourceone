﻿using System;
using Abacus.ReadModel.Repositories.Summary.Entities;
using Persistence;

namespace Abacus.ReadModel.Repositories.Summary.Queries
{
    public class QueryBankSummaryByBankId : GameDateBasedQueryParameter, IQueryParameters<BankSummaryEntity>
    {
        public QueryBankSummaryByBankId(DateTime gameDate, string shift, Guid bankId) : base(gameDate, shift)
        {
            BankId = bankId;
        }

        public Guid BankId { get; protected set; }
    }
}
