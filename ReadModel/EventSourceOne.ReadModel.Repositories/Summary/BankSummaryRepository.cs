﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using Abacus.ReadModel.Contracts.Summary.Dto;
using Abacus.ReadModel.Repositories.Summary.Entities;
using Abacus.ReadModel.Repositories.Summary.Queries;
using Dapper;
using Persistence;

namespace Abacus.ReadModel.Repositories.Summary
{
    public class BankSummaryRepository : AbstractRepository<BankSummaryEntity>
    {
        public BankSummaryRepository(IConnectionProvider connectionProvider) : base(connectionProvider)
        {
        }

        public override void Save(BankSummaryEntity entity)
        {
            SaveOrUpdate(entity, InsertSql, UpdateSql);
        }

        public override BankSummaryEntity Get(Guid id)
        {
            using (var conn = ConnectionProvider.GetConnection())
            {
                return conn.Query<BankSummaryEntity>(
                    @"SELECT * FROM BankSummary WHERE Id = @Id", new {id}).SingleOrDefault();
            }
        }

        public override IEnumerable<BankSummaryEntity> Get(params Guid[] ids)
        {
            using (var conn = ConnectionProvider.GetConnection())
            {
                return conn.Query<BankSummaryEntity>(
                    @"SELECT * FROM BankSummary WHERE Id in @Ids", new { ids }).ToList();
            }
        }

        public override void Delete(BankSummaryEntity entity)
        {
            using (var txn = new TransactionScope())
            using (var conn = ConnectionProvider.GetConnection())
            {
                conn.Execute("DELETE FROM BankSummary WHERE Id = @Id", entity);
                txn.Complete();
            }
        }

        public IEnumerable<Guid> RunQuery(QueryBankSummaryByBankId query)
        {
            using (var conn = ConnectionProvider.GetConnection())
            {
                return conn.Query<Guid>(
                    "SELECT Id FROM BankSummary WHERE BankId = @BankId AND GameDate = @GameDate AND Shift = @Shift",
                    query);
            }
        }

        private const string InsertSql = @"
            INSERT INTO BankSummary (Id, Version, BankId, Name, GamingDate, Shift, OpeningBalance, CurrentBalance, TotalAccountability) 
            VALUES (@Id, @Version + 1, @BankId, @Name, @GamingDate, @Shift, @OpeningBalance, @CurrentBalance, @TotalAccountability)";

        private const string UpdateSql = @"
            UPDATE BankSummary
            SET    Version = @Version + 1,
                   CurrentBalance = @CurrentBalance,
                   TotalAccountability = @TotalAccountability
            WHERE  Id = @Id
            AND    Version = @Version";
    }
}
