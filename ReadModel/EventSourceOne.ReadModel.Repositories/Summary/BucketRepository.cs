﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using Abacus.ReadModel.Repositories.Summary.Entities;
using Abacus.ReadModel.Repositories.Summary.Queries;
using Dapper;
using Persistence;

namespace Abacus.ReadModel.Repositories.Summary
{
    public class BucketRepository : AbstractRepository<BucketEntity>
    {
        public BucketRepository(IConnectionProvider connectionProvider) : base(connectionProvider)
        {
        }

        public override void Save(BucketEntity entity)
        {
            SaveOrUpdate(entity, InsertSql, UpdateSql);
        }

        public override BucketEntity Get(Guid id)
        {
            using (var conn = ConnectionProvider.GetConnection())
            {
                return conn.Query<BucketEntity>("SELECT * FROM Bucket WHERE Id = @id", new {id}).SingleOrDefault();
            }
        }

        public override IEnumerable<BucketEntity> Get(params Guid[] id)
        {
            using (var conn = ConnectionProvider.GetConnection())
            {
                return conn.Query<BucketEntity>("SELECT * FROM Bucket WHERE Id in @id", new { id }).ToList();
            }
        }

        public override void Delete(BucketEntity entity)
        {
            using (var txn = new TransactionScope())
            using (var conn = ConnectionProvider.GetConnection())
            {
                conn.Execute("DELETE FROM Bucket WHERE Id = @Id");
                txn.Complete();
            }
        }

        public IEnumerable<Guid> RunQuery(QueryBucketsByBank query)
        {
            using (var conn = ConnectionProvider.GetConnection())
            {
                return conn.Query<Guid>(
                    "SELECT Id FROM Bucket WHERE BankId = @BankId AND GameDate = @GameDate AND Shift = @Shift",
                    query);
            }
        }

        private const string InsertSql = 
          @"INSERT INTO Bucket(Id, Version, BankId, BucketId, GamingDate, Shift, Name, Type, OpeningValue, CurrentValue)
            VALUES (@Id, @Version + 1, @BankId, @BucketId, @GamingDate, @Shift, @Name, @Type, @OpeningValue, @CurrentValue)";

        private const string UpdateSql = 
          @"UPDATE  Bucket
            SET     CurrentValue = @CurrentValue,
                    Version = @Version + 1
            WHERE   Id = @Id
            AND     Version = @Version";
    }
}
