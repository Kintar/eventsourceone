﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using Abacus.ReadModel.Repositories.Summary.Entities;
using Abacus.ReadModel.Repositories.Summary.Queries;
using Dapper;
using Persistence;

namespace Abacus.ReadModel.Repositories.Summary
{
    public class LineItemRepository : AbstractRepository<LineItemEntity>
    {
        public LineItemRepository(IConnectionProvider connectionProvider) : base(connectionProvider)
        {
        }

        public override void Save(LineItemEntity entity)
        {
            SaveOrUpdate(entity, InsertSql, UpdateSql);
        }

        public override LineItemEntity Get(Guid id)
        {
            using (var conn = ConnectionProvider.GetConnection())
            {
                return conn.Query<LineItemEntity>("SELECT * FROM LineItem WHERE Id = @id", new {id}).SingleOrDefault();
            }
        }

        public override IEnumerable<LineItemEntity> Get(params Guid[] id)
        {
            using (var conn = ConnectionProvider.GetConnection())
            {
                return conn.Query<LineItemEntity>("SELECT * FROM LineItem WHERE Id in @id", new { id }).ToList();
            }
        }

        public override void Delete(LineItemEntity entity)
        {
            using (var txn = new TransactionScope())
            using (var conn = ConnectionProvider.GetConnection())
            {
                conn.Execute("DELETE FROM LineItem WHERE Id = @id", entity);
                txn.Complete();
            }
        }

        public IEnumerable<Guid> RunQuery(QueryLineItemsByBank query)
        {
            using (var conn = ConnectionProvider.GetConnection())
            {
                return conn.Query<Guid>(
                    "SELECT Id FROM LineItem WHERE BankId = @BankId AND GameDate = @GameDate AND Shift = @Shift",
                    query);
            }
        }

        public IEnumerable<Guid> RunQuery(QueryLineItemsByBucket query)
        {
            using (var conn = ConnectionProvider.GetConnection())
            {
                return conn.Query<Guid>(
                    "SELECT Id FROM LineItem WHERE BucketId = @BucketId AND GameDate = @GameDate AND Shift = @Shift",
                    query);
            }
        }

        public const string InsertSql =
          @"INSERT INTO LineItem(Id, Version, BankId, BucketId, GamingDate, Shift, Name, MediaSign, OpeningValue, CurrentValue)
            VALUES (@Id, @Version + 1, @BankId, @BucketId, @GamingDate, @Shift, @Name, @MediaSign, @OpeningValue, @CurrentValue)";

        public const string UpdateSql =
          @"UPDATE  LineItem
            SET     Version = @Version + 1";
    }
}
