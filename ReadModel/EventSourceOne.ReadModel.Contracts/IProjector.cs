﻿using System.Collections.Generic;

namespace Abacus.ReadModel.Contracts
{
    public interface IProjector
    {
        void Handle(EventMessage message);
    }
}
