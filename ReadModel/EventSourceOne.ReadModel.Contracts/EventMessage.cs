﻿using System;
using System.Runtime.Serialization;

namespace Abacus.ReadModel.Contracts
{
    [DataContract]
    public class EventMessage
    {
        [DataMember(Order = 0)]
        public string EventType { get; set; }

        [DataMember(Order = 1)]
        public string EventPayload { get; set; }
    }
}