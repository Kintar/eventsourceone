﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Abacus.ReadModel.Contracts.Summary.Dto
{
    public class BankSummary
    {
        public Guid Id { get; set; }
        public long Version { get; set; }
        public Guid BankId { get; set; }
        public DateTime GamingDate { get; set; }
        public string Shift { get; set; }
        public string Name { get; set; }
        public decimal OpeningBalance { get; set; }
        public decimal CurrentBalance { get; set; }
        public decimal TotalAccountability { get; set; }
    }
}
