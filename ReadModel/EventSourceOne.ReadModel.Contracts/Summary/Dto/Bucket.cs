﻿using System;
using BankDomain.Contracts.Values;

namespace Abacus.ReadModel.Contracts.Summary.Dto
{
    public class Bucket
    {
        public Guid Id { get; set; }
        public long Version { get; set; }
        public Guid BankId { get; set; }
        public DateTime GamingDate { get; set; }
        public string Shift { get; set; }
        public string Name { get; set; }
        public BucketType Type { get; set; }
        public decimal OpeningValue { get; set; }
        public decimal CurrentValue { get; set; }
    }
}