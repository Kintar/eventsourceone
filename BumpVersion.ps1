param(
	[string]$which,
	[string]$tag
)

$scriptPath = split-path -parent $MyInvocation.MyCommand.Definition
$fileText = [IO.File]::ReadAllText($scriptPath + "/version.txt").Trim().Split("_")
$version = $fileText[0]

$v = New-Object System.Version -ArgumentList $version

$ma = $v.Major
$mi = $v.Minor
$pa = $v.Build
$changeSuffix = 0

switch ($which)
{
	"major" { $ma++; $mi = 0; $pa = 0 }
	"minor" { $mi++; $pa = 0 }
	"patch" { $pa++ }
	"suffix" { $changeSuffix = 1 }
}

$v = New-Object System.Version $ma,$mi,$pa,0

$suffix = ""
If ($fileText.Length -eq 2) {
	$suffix = "_" + $fileText[1]
}

if ($changeSuffix -eq 1) {
	$suffix = "_" + $tag
}

$content = $v.ToString() + $suffix

Set-Content ($scriptPath + "/version.txt") $content

$versionFile = $scriptPath + "/version.txt"

git add $versionFile
git commit -m "Bumped $which version"
$fullVersion = "" + $v + $suffix
git tag $fullVersion

$message = "Version is now " + $v + $suffix

Write-Host -Foreground green $message