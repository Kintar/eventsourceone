using System;
using System.Collections.Generic;
using System.Linq;
using BankDomain.Contracts.Events;
using BankDomain.Contracts.Values;
using BankDomain.Exceptions;
using FluentAssertions;
using Xunit;

namespace BankDomain.Tests
{
    public class AbacusBankLocationTests : AbacusBankTestsBase
    {
        private const string LocationName = "ACCOUNTING DEPT";

        [Fact]
        public void CanCreateExternalLocation()
        {
            var bank = new AbacusBank(EventHandlerRegistry);

            bank.Create("Test Bank");
            var bucket = bank.CreateBucket("ACCOUNTING", BucketType.Accountability);
            var sendLine = bank.CreateLineItem("TEST SEND", bucket, MediaSign.Negative, 0.01m);
            var recvLine = bank.CreateLineItem("TEST RECV", bucket, MediaSign.Positive, 0.01m);
            
            bank.ClearUncomittedEvents();

            var id = bank.CreateExternalLocation("ACCOUNTING DEPT", sendLine, recvLine);
            id.Should().NotBe(Guid.Empty);
            var evts = bank.GetUncommittedEvents().ToList();
            evts.Count.ShouldBeEquivalentTo(1);
            var evt = evts.Single() as ExternalLocationCreated;

            Assert.NotNull(evt);
            evt.LocationId.ShouldBeEquivalentTo(id);
            evt.SendLineId.ShouldBeEquivalentTo(sendLine);
            evt.ReceiveLineId.ShouldBeEquivalentTo(recvLine);
        }

        [Fact]
        public void CannotCreateDuplicateNamedExternalLocation()
        {
            var bank = new AbacusBank(EventHandlerRegistry);

            bank.Create("Test Bank");
            var bucket = bank.CreateBucket("ACCOUNTING", BucketType.Accountability);
            var sendLine = bank.CreateLineItem("TEST", bucket, MediaSign.Positive, 0.01m);
            var recvLine = bank.CreateLineItem("TEST RECV", bucket, MediaSign.Negative, 0.01m);
            var line2 = bank.CreateLineItem("OTHER ITEM", bucket, MediaSign.Positive, 0.01m);
            bank.ClearUncomittedEvents();

            bank.CreateExternalLocation(LocationName, sendLine, recvLine);

            Action act = () => bank.CreateExternalLocation(LocationName, sendLine, recvLine);
            act.ShouldThrow<DuplicateLocationException>()
                .And.Message.Contains(LocationName).Should().BeTrue();

            act = () => bank.CreateExternalLocation(LocationName, line2, recvLine);
            act.ShouldThrow<DuplicateLocationException>()
                .And.Message.Contains(LocationName).Should().BeTrue();
        }

        [Fact]
        public void CanDisableExternalLocation()
        {
            var bank = new AbacusBank(EventHandlerRegistry);

            bank.Create("Test Bank");
            var bucket = bank.CreateBucket("ACCOUNTING", BucketType.Accountability);
            var sendLine = bank.CreateLineItem("TEST", bucket, MediaSign.Positive, 0.01m);
            var recvLine = bank.CreateLineItem("TEST RECV", bucket, MediaSign.Negative, 0.01m);

            var id = bank.CreateExternalLocation(LocationName, sendLine, recvLine);
            bank.ClearUncomittedEvents();

            bank.DisableExternalLocation(id);
            var evts = bank.GetUncommittedEvents().ToList();
            evts.Count.ShouldBeEquivalentTo(1);
            var evt = evts.Single() as ExternalLocationActiveChanged;

            Assert.NotNull(evt);
            evt.LocationId.ShouldBeEquivalentTo(id);
            evt.NewState.ShouldBeEquivalentTo(false);
        }

        [Fact]
        public void CanEnableExternalLocation()
        {
            var bank = new AbacusBank(EventHandlerRegistry);

            bank.Create("Test Bank");
            var bucket = bank.CreateBucket("ACCOUNTING", BucketType.Accountability);
            var line = bank.CreateLineItem("TEST", bucket, MediaSign.Positive, 0.01m);
            var recvLine = bank.CreateLineItem("TEST RECV", bucket, MediaSign.Negative, 0.01m);

            var id = bank.CreateExternalLocation(LocationName, line, recvLine);
            bank.DisableExternalLocation(id);
            bank.ClearUncomittedEvents();

            bank.EnableExternalLocation(id);
            var evts = bank.GetUncommittedEvents().ToList();
            evts.Count.ShouldBeEquivalentTo(1);
            var evt = evts.Single() as ExternalLocationActiveChanged;

            Assert.NotNull(evt);
            evt.LocationId.ShouldBeEquivalentTo(id);
            evt.NewState.ShouldBeEquivalentTo(true);
        }

        [Fact]
        public void CanReceiveFromExternalLocation()
        {
            Guid oneDollar;
            Guid fiveDollar;
            Guid accItem;
            var bank = SetupInitialBank(out oneDollar, out fiveDollar, out accItem);

            var bucket = bank.CreateBucket("TEST LOCATIONS", BucketType.Accountability);
            var line = bank.CreateLineItem("ACCOUNTING", bucket, MediaSign.Negative, 0.01m);
            var recvLine = bank.CreateLineItem("TEST RECV", bucket, MediaSign.Positive, 0.01m);

            var id = bank.CreateExternalLocation(LocationName, line, recvLine);
            bank.ClearUncomittedEvents();

            var receipt = new List<LineItemAmount>
            {
                new LineItemAmount(oneDollar, 5),
                new LineItemAmount(fiveDollar, 25)
            };

            bank.ReceiveFromLocation(id, receipt);
            var evts = bank.GetUncommittedEvents().ToList();

            evts.Count.ShouldBeEquivalentTo(2);
            var transferEvt = evts.OfType<TransferredWithLocation>().FirstOrDefault();
            var balanceEvt = evts.OfType<BalanceChanged>().FirstOrDefault();

            Assert.NotNull(transferEvt);
            Assert.NotNull(balanceEvt);

            transferEvt.BankBalanceChange.ShouldBeEquivalentTo(receipt.Sum(x => x.Value));

            balanceEvt.InventoryChanges.ShouldBeEquivalentTo(receipt);
        }

        [Fact]
        public void CanSendToExternalLocation()
        {
            Guid oneDollar;
            Guid fiveDollar;
            Guid accItem;
            var bank = SetupInitialBank(out oneDollar, out fiveDollar, out accItem);

            var bucket = bank.CreateBucket("TEST LOCATIONS", BucketType.Accountability);
            var line = bank.CreateLineItem("ACCOUNTING", bucket, MediaSign.Negative, 0.01m);
            var recvLine = bank.CreateLineItem("TEST RECV", bucket, MediaSign.Positive, 0.01m);

            var id = bank.CreateExternalLocation(LocationName, line, recvLine);
            bank.AdjustBalance(accItem, new [] { new LineItemAmount(fiveDollar, 25) });
            bank.ClearUncomittedEvents();

            var receipt = new List<LineItemAmount>
            {
                new LineItemAmount(oneDollar, 5),
                new LineItemAmount(fiveDollar, 25)
            };

            bank.SendToLocation(id, receipt);
            var evts = bank.GetUncommittedEvents().ToList();

            evts.Count.ShouldBeEquivalentTo(2);
            var transferEvt = evts.OfType<TransferredWithLocation>().FirstOrDefault();
            var balanceEvt = evts.OfType<BalanceChanged>().FirstOrDefault();

            Assert.NotNull(transferEvt);
            Assert.NotNull(balanceEvt);

            transferEvt.BankBalanceChange.ShouldBeEquivalentTo(receipt.Sum(x => -x.Value));

            var changes = balanceEvt.InventoryChanges.ToList();
            var changedLines = changes.Select(x => x.LineId).ToList();
            changedLines.ShouldBeEquivalentTo(receipt.Select(x => x.LineId));
            changes.Sum(x => x.Value).ShouldBeEquivalentTo(receipt.Sum(x => -x.Value));
        }

        [Fact]
        public void CannotSendToDisabledExternalLocation()
        {
            Guid oneDollar;
            Guid fiveDollar;
            Guid accItem;
            var bank = SetupInitialBank(out oneDollar, out fiveDollar, out accItem);

            var bucket = bank.CreateBucket("TEST LOCATIONS", BucketType.Accountability);
            var line = bank.CreateLineItem("ACCOUNTING", bucket, MediaSign.Negative, 0.01m);
            var recvLine = bank.CreateLineItem("TEST RECV", bucket, MediaSign.Positive, 0.01m);

            var id = bank.CreateExternalLocation(LocationName, line, recvLine);
            bank.DisableExternalLocation(id);
            bank.ClearUncomittedEvents();

            var receipt = new List<LineItemAmount>
            {
                new LineItemAmount(oneDollar, 5),
                new LineItemAmount(fiveDollar, 25)
            };

            Action act = () =>
                bank.SendToLocation(id, receipt);

            act.ShouldThrow<InvalidBankOperationException>();
        }

        [Fact]
        public void CannotReceiveFromDisabledExternalLocation()
        {
            Guid oneDollar;
            Guid fiveDollar;
            Guid accItem;
            var bank = SetupInitialBank(out oneDollar, out fiveDollar, out accItem);

            var bucket = bank.CreateBucket("TEST LOCATIONS", BucketType.Accountability);
            var line = bank.CreateLineItem("ACCOUNTING", bucket, MediaSign.Negative, 0.01m);
            var recvLine = bank.CreateLineItem("TEST RECV", bucket, MediaSign.Positive, 0.01m);

            var id = bank.CreateExternalLocation(LocationName, line, recvLine);
            bank.DisableExternalLocation(id);
            bank.ClearUncomittedEvents();

            var receipt = new List<LineItemAmount>
            {
                new LineItemAmount(oneDollar, 5),
                new LineItemAmount(fiveDollar, 25)
            };

            Action act = () =>
                bank.ReceiveFromLocation(id, receipt);

            act.ShouldThrow<InvalidBankOperationException>();
        }

        [Fact]
        public void DisablingExternalLocationDoesNotDisableAccountabilityLine()
        {
            var bank = new AbacusBank(EventHandlerRegistry);

            bank.Create("Test Bank");
            var bucket = bank.CreateBucket("ACCOUNTING", BucketType.Accountability);
            var line = bank.CreateLineItem("TEST", bucket, MediaSign.Positive, 0.01m);
            var recvLine = bank.CreateLineItem("TEST RECV", bucket, MediaSign.Positive, 0.01m);

            var id = bank.CreateExternalLocation(LocationName, line, recvLine);
            bank.DisableExternalLocation(id);

            bank.LineItems.Single(x => x.Id == line).Active.Should().BeTrue();
        }
    }
}