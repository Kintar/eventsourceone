using System;
using System.Collections.Generic;
using System.Linq;
using BankDomain.Contracts.Events;
using BankDomain.Contracts.Values;
using BankDomain.Exceptions;
using FluentAssertions;
using Xunit;

namespace BankDomain.Tests
{
    public class AbacusBankBalanceTests : AbacusBankTestsBase
    {
        [Fact]
        public void CanAdjustBankBalance()
        {
            var bank = new AbacusBank(EventHandlerRegistry);

            bank.Create("Test Bank");
            var invBucket = bank.CreateBucket("Inventory", BucketType.Inventory);
            var acctBucket = bank.CreateBucket("Accountability", BucketType.Accountability);
            var oneDollar = bank.CreateLineItem("$1", invBucket, MediaSign.Positive, 1);
            var fiveDollar = bank.CreateLineItem("$5", invBucket, MediaSign.Positive, 5);
            var accItem = bank.CreateLineItem("Received From Bank", acctBucket, MediaSign.Positive, .01m);

            bank.ClearUncomittedEvents();

            var receipt = new List<LineItemAmount>
            {
                new LineItemAmount(oneDollar, 100),
                new LineItemAmount(fiveDollar, 500)
            };

            bank.AdjustBalance(accItem, receipt);

            var evt = bank.GetUncommittedEvents().Single() as BalanceChanged;
            Assert.NotNull(evt);

            evt.AccountabilityLineId.ShouldBeEquivalentTo(accItem);
            evt.InventoryChanges.Count().ShouldBeEquivalentTo(2);

            var ones = bank.LineItems.Single(x => x.Id == oneDollar);
            var fives = bank.LineItems.Single(x => x.Id == fiveDollar);
            var acc = bank.LineItems.Single(x => x.Id == accItem);

            ones.Value.ShouldBeEquivalentTo(100);
            fives.Value.ShouldBeEquivalentTo(500);
            acc.Value.ShouldBeEquivalentTo(600);

            receipt = new List<LineItemAmount>
            {
                new LineItemAmount(oneDollar, -50),
            };

            bank.ClearUncomittedEvents();
            bank.AdjustBalance(accItem, receipt);
            acc.Value.ShouldBeEquivalentTo(550);
            ones.Value.ShouldBeEquivalentTo(50);
        }

        [Fact]
        public void CannotAdjustBalanceWithBadUnit()
        {
            Guid oneDollar;
            Guid fiveDollar;
            Guid accItem;
            var bank = SetupInitialBank(out oneDollar, out fiveDollar, out accItem);

            var receipt = new List<LineItemAmount>
            {
                new LineItemAmount(oneDollar, 100),
                new LineItemAmount(fiveDollar, 502)
            };

            Action act = () => bank.AdjustBalance(accItem, receipt);
            act.ShouldThrow<InvalidInventoryUnitException>();
            bank.GetUncommittedEvents().Count().ShouldBeEquivalentTo(0);
        }

        [Fact]
        public void CannotAdjustBalanceWithInsufficientInventory()
        {
            Guid oneDollar;
            Guid fiveDollar;
            Guid accItem;
            var bank = SetupInitialBank(out oneDollar, out fiveDollar, out accItem);

            var receipt = new List<LineItemAmount>
            {
                new LineItemAmount(oneDollar, -100),
                new LineItemAmount(fiveDollar, 500)
            };

            Action act = () => bank.AdjustBalance(accItem, receipt);
            act.ShouldThrow<InsufficientBalanceException>();
            bank.GetUncommittedEvents().Count().ShouldBeEquivalentTo(0);
        }

        [Fact]
        public void CannotUseDisabledLineInAdjustment()
        {
            Guid oneDollar;
            Guid fiveDollar;
            Guid accItem;
            var bank = SetupInitialBank(out oneDollar, out fiveDollar, out accItem);
            bank.AdjustBalance(accItem, new [] { new LineItemAmount(oneDollar, -50)} );
            bank.DisableLineItem(oneDollar);

            var receipt = new List<LineItemAmount>
            {
                new LineItemAmount(oneDollar, 100),
                new LineItemAmount(fiveDollar, 500)
            };

            Action act = () => bank.AdjustBalance(accItem, receipt);
            act.ShouldThrow<InvalidBankOperationException>();
        }

        [Fact]
        public void CanMoveValueBetweenAccounts()
        {
            Guid oneDollar;
            Guid fiveDollar;
            Guid accItem;
            var bank = SetupInitialBank(out oneDollar, out fiveDollar, out accItem);

            bank.MoveValue(oneDollar, fiveDollar, 25);
            var evt = bank.GetUncommittedEvents().Single() as ValueMoved;

            Assert.NotNull(evt);
            evt.Adjustments.Count().ShouldBeEquivalentTo(2);

            var oneDollarItem = bank.LineItems.Single(x => x.Id == oneDollar);
            var fiveDollarItem = bank.LineItems.Single(x => x.Id == fiveDollar);

            oneDollarItem.Value.ShouldBeEquivalentTo(25);
            fiveDollarItem.Value.ShouldBeEquivalentTo(25);
        }

        [Fact]
        public void CannotMoveValueInInvalidUnits()
        {
            Guid oneDollar;
            Guid fiveDollar;
            Guid accItem;
            var bank = SetupInitialBank(out oneDollar, out fiveDollar, out accItem);

            Action act = () => bank.MoveValue(oneDollar, fiveDollar, 7);

            act.ShouldThrow<InvalidInventoryUnitException>();
        }

        [Fact]
        public void CannotMoveValueFromInventoryToAccountability()
        {
            Guid oneDollar;
            Guid fiveDollar;
            Guid accItem;
            var bank = SetupInitialBank(out oneDollar, out fiveDollar, out accItem);

            Action act = () => bank.MoveValue(oneDollar, accItem, 10);

            act.ShouldThrow<TransactionBalanceException>();
        }

        [Fact]
        public void CannotMoveValueBeyondAvailiability()
        {
            Guid oneDollar;
            Guid fiveDollar;
            Guid accItem;
            var bank = SetupInitialBank(out oneDollar, out fiveDollar, out accItem);

            Action act = () => bank.MoveValue(oneDollar, fiveDollar, 7000);

            act.ShouldThrow<InsufficientBalanceException>();
        }

        [Fact]
        public void CanSettleBank()
        {
            Guid oneDollar;
            Guid fiveDollar;
            Guid accItem;
            var bank = SetupInitialBank(out oneDollar, out fiveDollar, out accItem);

            bank.OpeningBalance.ShouldBeEquivalentTo(0);

            bank.SettleBank();
            bank.OpeningBalance.ShouldBeEquivalentTo(50);
            bank.Buckets.Where(x => x.Type == BucketType.Accountability)
                .Sum(x => x.Items.Sum(y => y.Value))
                .ShouldBeEquivalentTo(0);
        }
    }
}