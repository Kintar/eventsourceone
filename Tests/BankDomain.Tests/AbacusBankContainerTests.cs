﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using BankDomain.Contracts.Events;
using BankDomain.Contracts.Values;
using BankDomain.Entities;
using BankDomain.Exceptions;
using FluentAssertions;
using Xunit;

namespace BankDomain.Tests
{
    public class AbacusBankContainerTests : AbacusBankTestsBase
    {
        [Fact]
        public void CanCreateContainerDefinition()
        {
            AbacusBank bank;
            Guid prepLine;
            Guid outLine;
            List<LineItemAmount> items;
            var defId = CreateBasicContainerDefinition(out bank, out prepLine, out outLine, out items);
            var evt = bank.GetUncommittedEvents().SingleOrDefault() as ContainerDefinitionCreated;

            Assert.NotNull(evt);
            evt.ContainerDefinitionId.ShouldBeEquivalentTo(defId);
            evt.OutLineId.ShouldBeEquivalentTo(outLine);
            evt.PrepLineId.ShouldBeEquivalentTo(prepLine);
            evt.Media.ShouldBeEquivalentTo(items);
            evt.Name.ShouldBeEquivalentTo("TEST");
            evt.Type.ShouldBeEquivalentTo("TEST");
        }

        private Guid CreateBasicContainerDefinition(out AbacusBank bank, out Guid prepLine, out Guid outLine, out List<LineItemAmount> items)
        {
            Guid oneDollar;
            Guid fiveDollar;
            Guid accItem;
            bank = SetupInitialBank(out oneDollar, out fiveDollar, out accItem);
            var bucket = bank.Buckets.First(x => x.Type == BucketType.Inventory).Id;

            prepLine = bank.CreateLineItem("Prep", bucket, MediaSign.Positive, 0.01m);
            outLine = bank.CreateLineItem("Out", bucket, MediaSign.Positive, 0.01m);

            bank.ClearUncomittedEvents();

            items = new List<LineItemAmount>
            {
                new LineItemAmount(oneDollar, 10m),
                new LineItemAmount(fiveDollar, 50m)
            };

            var defId = bank.CreateContainerDefinition("TEST", "TEST", items, prepLine, outLine);
            return defId;
        }

        [Fact]
        public void CanDisableContainerDefinition()
        {
            AbacusBank bank;
            Guid prepLine;
            Guid outLine;
            List<LineItemAmount> items;
            var defId = CreateBasicContainerDefinition(out bank, out prepLine, out outLine, out items);
            bank.ClearUncomittedEvents();
            bank.DisableContainerDefinition(defId);
            var evt = bank.GetUncommittedEvents().SingleOrDefault() as ContainerDefinitionActiveChanged;

            Assert.NotNull(evt);
            evt.DefinitionId.ShouldBeEquivalentTo(defId);
            evt.NewState.Should().BeFalse();
        }

        [Fact]
        public void CanEnableContainerDefinition()
        {
            AbacusBank bank;
            Guid prepLine;
            Guid outLine;
            List<LineItemAmount> items;
            var defId = CreateBasicContainerDefinition(out bank, out prepLine, out outLine, out items);
            bank.DisableContainerDefinition(defId);
            bank.ClearUncomittedEvents();

            bank.EnableContainerDefinition(defId);
            var evt = bank.GetUncommittedEvents().SingleOrDefault() as ContainerDefinitionActiveChanged;

            Assert.NotNull(evt);
            evt.DefinitionId.ShouldBeEquivalentTo(defId);
            evt.NewState.Should().BeTrue();
        }

        [Fact]
        public void CanCreateContainer()
        {
            AbacusBank bank;
            Guid prepLine;
            Guid outLine;
            List<LineItemAmount> items;
            var defId = CreateBasicContainerDefinition(out bank, out prepLine, out outLine, out items);

            var accountabilityLine = bank.LineItems.Single(x => x.Bucket.Type == BucketType.Accountability);
            bank.AdjustBalance(accountabilityLine.Id, items);
            bank.ClearUncomittedEvents();

            var containers = bank.CreateContainer(defId, 1).ToList();
            var evt = bank.GetUncommittedEvents().OfType<ContainerCreated>().SingleOrDefault();

            Assert.NotNull(evt);
            evt.ContainerId.ShouldBeEquivalentTo(containers.First());
            evt.DefinitionId.ShouldBeEquivalentTo(defId);

            var transfers = bank.GetUncommittedEvents().OfType<ValueMoved>().SingleOrDefault();
            Assert.NotNull(transfers);
            var adjustments = transfers.Adjustments.ToDictionary(x => x.LineId, y => y.Value);
            foreach (var item in items)
            {
                adjustments.ContainsKey(item.LineId).Should().BeTrue();
                adjustments[item.LineId].ShouldBeEquivalentTo(-item.Value);
            }

            adjustments[prepLine].ShouldBeEquivalentTo(items.Sum(x => x.Value));
        }

        [Fact]
        public void CanReclaimContainer()
        {
            AbacusBank bank;
            Guid prepLine;
            Guid outLine;
            List<LineItemAmount> items;
            var defId = CreateBasicContainerDefinition(out bank, out prepLine, out outLine, out items);

            var accountabilityLine = bank.LineItems.Single(x => x.Bucket.Type == BucketType.Accountability);
            bank.AdjustBalance(accountabilityLine.Id, items);
            var containers = bank.CreateContainer(defId, 1).ToList();
            bank.ClearUncomittedEvents();

            bank.ReclaimContainer(defId, 1);
            var moveEvt = bank.GetUncommittedEvents().OfType<ValueMoved>().SingleOrDefault();
            var evt = bank.GetUncommittedEvents().OfType<ContainerReclaimed>().SingleOrDefault();

            Assert.NotNull(moveEvt);
            Assert.NotNull(evt);

            evt.ContainerId.ShouldBeEquivalentTo(containers.First());
            evt.DefinitionId.ShouldBeEquivalentTo(defId);

            var adjustments = moveEvt.Adjustments.ToDictionary(x => x.LineId, y => y.Value);
            items.All(x => adjustments.ContainsKey(x.LineId) && adjustments[x.LineId] == x.Value).Should().BeTrue();

            adjustments[prepLine].ShouldBeEquivalentTo(-items.Sum(x => x.Value));
        }

        [Fact]
        public void CannotCreateDuplicateContainerDefinition()
        {
            AbacusBank bank;
            Guid prepLine;
            Guid outLine;
            List<LineItemAmount> items;
            CreateBasicContainerDefinition(out bank, out prepLine, out outLine, out items);

            Action act = () => bank.CreateContainerDefinition("TEST", "TEST", items, prepLine, outLine);
            act.ShouldThrow<DuplicateContainerDefinitionException>();
        }

        [Fact]
        public void ContainerDuplicationConsidersTypeAndName()
        {
            AbacusBank bank;
            Guid prepLine;
            Guid outLine;
            List<LineItemAmount> items;
            CreateBasicContainerDefinition(out bank, out prepLine, out outLine, out items);

            Action act = () => bank.CreateContainerDefinition("TEST", "TEST2", items, prepLine, outLine);
            act.ShouldNotThrow();
            act = () => bank.CreateContainerDefinition("TEST2", "TEST", items, prepLine, outLine);
            act.ShouldNotThrow();
        }

        [Fact]
        public void CannotCreateContainerFromDisabledDefinition()
        {
            AbacusBank bank;
            Guid prepLine;
            Guid outLine;
            List<LineItemAmount> items;
            var id = CreateBasicContainerDefinition(out bank, out prepLine, out outLine, out items);
            bank.DisableContainerDefinition(id);
            var accountabilityLine = bank.LineItems.Single(x => x.Bucket.Type == BucketType.Accountability);
            bank.AdjustBalance(accountabilityLine.Id, items);

            Action act = () => bank.CreateContainer(id, 1);
            act.ShouldThrow<InvalidContainerDefinitionException>()
                .And.Message.Should().EndWith("inactive");
        }

        [Fact]
        public void CannotCreateContainerWithInsufficientInventory()
        {
            AbacusBank bank;
            Guid prepLine;
            Guid outLine;
            List<LineItemAmount> items;
            var id = CreateBasicContainerDefinition(out bank, out prepLine, out outLine, out items);

            Action act = () => bank.CreateContainer(id, 1);
            act.ShouldThrow<InsufficientBalanceException>();
        }
    }
}