using System;
using System.Linq;
using BankDomain.Contracts.Events;
using BankDomain.Contracts.Values;
using BankDomain.Exceptions;
using FluentAssertions;
using Xunit;

namespace BankDomain.Tests
{
    public class AbacusBankLineItemTests : AbacusBankTestsBase
    {
        [Fact]
        public void CanCreateLineItems()
        {
            var bank = new AbacusBank(EventHandlerRegistry);

            bank.Create("Test Bank");
            bank.CreateBucket("Test Bucket", BucketType.Inventory);
            bank.ClearUncomittedEvents();

            var bucket = bank.Buckets.Single();
            bank.CreateLineItem("Test Line", bucket.Id, MediaSign.Positive, 1m);

            var evt = bank.GetUncommittedEvents().Single() as LineItemCreated;

            Assert.NotNull(evt);

            var lineItem = bank.LineItems.Single();

            bucket.Items.Single().ShouldBeEquivalentTo(lineItem);
            lineItem.Name.ShouldBeEquivalentTo("Test Line");
            lineItem.Sign.ShouldBeEquivalentTo(MediaSign.Positive);
            lineItem.Unit.ShouldBeEquivalentTo(1m);
            lineItem.Bucket.ShouldBeEquivalentTo(bucket);
            lineItem.Active.Should().BeTrue();
        }

        [Fact]
        public void CannotCreateLineItemInNonexistentBucket()
        {
            var bank = new AbacusBank(EventHandlerRegistry);

            bank.Create("Test Bank");
            bank.CreateBucket("Test Bucket", BucketType.Inventory);
            bank.ClearUncomittedEvents();

            Action act = () => bank.CreateLineItem("Test", Guid.NewGuid(), MediaSign.Positive, 1m);
            act.ShouldThrow<BucketNotFoundException>();
        }

        [Fact]
        public void CanDisableLine()
        {
            var bank = new AbacusBank(EventHandlerRegistry);

            bank.Create("Test Bank");
            bank.CreateBucket("Test Bucket", BucketType.Inventory);

            var bucket = bank.Buckets.Single();
            bank.CreateLineItem("Test Line", bucket.Id, MediaSign.Positive, 1m);

            var line = bank.LineItems.Single();

            bank.ClearUncomittedEvents();
            bank.DisableLineItem(line.Id);

            var evt = bank.GetUncommittedEvents().Single() as LineItemActiveChanged;

            Assert.NotNull(evt);

            line.Active.Should().BeFalse();
        }

        [Fact]
        public void CanEnableLine()
        {
            var bank = new AbacusBank(EventHandlerRegistry);

            bank.Create("Test Bank");
            bank.CreateBucket("Test Bucket", BucketType.Inventory);

            var bucket = bank.Buckets.Single();
            bank.CreateLineItem("Test Line", bucket.Id, MediaSign.Positive, 1m);

            var line = bank.LineItems.Single();

            bank.DisableLineItem(line.Id);
            bank.ClearUncomittedEvents();
            bank.EnableLineItem(line.Id);

            var evt = bank.GetUncommittedEvents().Single() as LineItemActiveChanged;

            Assert.NotNull(evt);

            line.Active.Should().BeTrue();
        }

        [Fact]
        public void CannotDisableLineWithExistingBalance()
        {
            Guid oneDollar;
            Guid fiveDollar;
            Guid accItem;
            var bank = SetupInitialBank(out oneDollar, out fiveDollar, out accItem);

            Action act = () => bank.DisableLineItem(oneDollar);
            act.ShouldThrow<LineItemNotEmptyException>();
        }
    }
}