﻿using System;
using System.Linq;
using System.Runtime;
using System.Text;
using System.Threading.Tasks;
using BankDomain.Contracts.Events;
using BankDomain.Contracts.Values;
using BankDomain.Entities;
using BankDomain.Exceptions;
using FluentAssertions;
using Xunit;

namespace BankDomain.Tests
{
    public class AbacusBankTests : AbacusBankTestsBase
    {
        [Fact]
        public void CanCreateBanks()
        {
            var bank = new AbacusBank(EventHandlerRegistry);

            bank.Create("Test Bank");

            bank.Id.Should().NotBe(Guid.Empty);
            bank.Name.ShouldBeEquivalentTo("Test Bank");
            bank.Version.ShouldBeEquivalentTo(1);
            bank.OpeningBalance.ShouldBeEquivalentTo(0);

            var events = bank.GetUncommittedEvents().ToList();

            events.Count.ShouldBeEquivalentTo(1);

            var creationEvent = events.Single() as BankCreated;

            Assert.NotNull(creationEvent);
            creationEvent.BankId.ShouldBeEquivalentTo(bank.Id);
            creationEvent.Name.ShouldBeEquivalentTo(bank.Name);
        }

        [Fact]
        public void CanCreateBuckets()
        {
            var bank = new AbacusBank(EventHandlerRegistry);

            bank.Create("Test Bank");
            bank.ClearUncomittedEvents();

            bank.CreateBucket("Test Bucket", BucketType.Inventory);

            var buckets = bank.Buckets.ToList();
            buckets.Count.ShouldBeEquivalentTo(1);

            var evt = bank.GetUncommittedEvents().Single() as BucketCreated;

            Assert.NotNull(evt);

            var bucket = bank.Buckets.Single();

            bucket.Id.ShouldBeEquivalentTo(evt.BucketId);
            bucket.Name.ShouldBeEquivalentTo("Test Bucket");
            bucket.Type.ShouldBeEquivalentTo(BucketType.Inventory);
            bucket.Items.Count().ShouldBeEquivalentTo(0);
        }
    }
}
