﻿using System;
using System.Collections.Generic;
using BankDomain.Contracts.Values;
using Common.Domain;

namespace BankDomain.Tests
{
    public class AbacusBankTestsBase
    {
        protected readonly EventHandlerRegistry EventHandlerRegistry = new EventHandlerRegistry();

        protected AbacusBank SetupInitialBank(out Guid oneDollar, out Guid fiveDollar, out Guid accItem)
        {
            var bank = new AbacusBank(EventHandlerRegistry);

            bank.Create("Test Bank");
            var invBucket = bank.CreateBucket("Inventory", BucketType.Inventory);
            var acctBucket = bank.CreateBucket("Accountability", BucketType.Accountability);
            oneDollar = bank.CreateLineItem("$1", invBucket, MediaSign.Positive, 1);
            fiveDollar = bank.CreateLineItem("$5", invBucket, MediaSign.Positive, 5);
            accItem = bank.CreateLineItem("Received From Bank", acctBucket, MediaSign.Positive, .01m);

            var receipt = new List<LineItemAmount>
            {
                new LineItemAmount(oneDollar, 50)
            };
            bank.AdjustBalance(accItem, receipt);
            bank.ClearUncomittedEvents();
            return bank;
        }
    }
}