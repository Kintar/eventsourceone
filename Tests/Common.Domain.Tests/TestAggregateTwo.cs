﻿namespace Common.Domain.Tests
{
    public class RandomFactoryThing
    {
    }

    public class TestAggregateTwo : Aggregate
    {
        public RandomFactoryThing Thing { get; protected set; }

        public TestAggregateTwo(EventHandlerRegistry eventRegistry, RandomFactoryThing thing) : base(eventRegistry)
        {
            Thing = thing;
        }
    }
}