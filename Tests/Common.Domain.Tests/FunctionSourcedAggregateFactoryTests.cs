﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Autofac;
using Autofac.Core;
using Autofac.Features.GeneratedFactories;
using Autofac.Features.ResolveAnything;
using Xunit;

namespace Common.Domain.Tests
{
    public class FunctionSourcedAggregateFactoryTests
    {
        protected IContainer BuildContainer()
        {
            var builder = new ContainerBuilder();

            var assembly = Assembly.GetAssembly(typeof (TestAggregateOne));
            var aggregateTypes = assembly.GetTypes().Where(t => t.IsAssignableTo<IAggregate>() && !t.IsInterface && !t.IsAbstract);

            builder.RegisterSource(new AnyConcreteTypeNotAlreadyRegisteredSource());

            builder.Register(c =>
            {
                var funcs = new Dictionary<Type, Func<IAggregate>>();
                foreach (var t in aggregateTypes)
                {
                    funcs[t] = (Func<IAggregate>) c.Resolve(typeof(Func<>).MakeGenericType(t));
                }

                return new FunctionSourcedAggregateFactory(funcs);
            }).SingleInstance()
                .AsImplementedInterfaces();

            return builder.Build();
        }

        [Fact]
        public void AggregateFactoryFunctions()
        {
            var factory = BuildContainer().Resolve<IAggregateFactory>();

            var agg1 = factory.CreateInstance<TestAggregateOne>();
            var agg2 = factory.CreateInstance<TestAggregateTwo>();

            Assert.NotNull(agg1);
            Assert.NotNull(agg2);

            Assert.NotNull(agg2.Thing);
        }
    }
}
