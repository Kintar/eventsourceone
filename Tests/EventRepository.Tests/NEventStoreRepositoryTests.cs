﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Threading.Tasks;
using Common.Domain;
using EventRepository.Tests.TestModel;
using FluentAssertions;
using NEventStore;
using Xunit;

namespace EventRepository.Tests
{
    public class NEventStoreRepositoryTests
    {
        protected IStoreEvents WireEventStore()
        {
            return Wireup.Init()
                .LogToOutputWindow()
                .UsingInMemoryPersistence()
                .InitializeStorageEngine()
                .UsingJsonSerialization()
                .Build();
        }

        static FunctionSourcedAggregateFactory factory;

        protected IStoreAggregates GetRepository()
        {
            if (factory == null)
            {
                var registry = new EventHandlerRegistry();
                var dict = new Dictionary<Type, Func<IAggregate>>();

                dict[typeof (SomeAggregate)] = () => new SomeAggregate(registry);

                factory = new FunctionSourcedAggregateFactory(dict);
            }

            return new NeventStoreAggregateRepository(WireEventStore(), factory);
        }

        [Fact]
        public void CanSaveAndLoadAggregates()
        {
            var repo = GetRepository();

            var agg = factory.CreateInstance<SomeAggregate>();
            agg.Create("TEST ONE");
            agg.AdjustValue((decimal)new Random().NextDouble() * 1000m);
            agg.IsTransient.Should().BeTrue();
            repo.Save(agg);
            agg.IsTransient.Should().BeFalse();

            agg.GetUncommittedEvents().Any().Should().BeFalse();

            var lazarus = repo.Load<SomeAggregate>(agg.Id);

            lazarus.Name.ShouldBeEquivalentTo(agg.Name);
            lazarus.Id.ShouldBeEquivalentTo(agg.Id);
            lazarus.Value.ShouldBeEquivalentTo(agg.Value);
        }
    }
}
