﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common.Domain;

namespace EventRepository.Tests.TestModel
{
    public class SomeAggregate : Aggregate
    {
        public string Name { get; protected set; }
        public decimal Value { get; protected set; }

        public SomeAggregate(EventHandlerRegistry eventRegistry) : base(eventRegistry)
        {
        }

        public void Create(string name)
        {
            if (Id != Guid.Empty)
            {
                throw new AggregateException("Already created");
            }

            RaiseEvent(new Created(Guid.NewGuid(), name));
        }

        protected void Apply(Created evt)
        {
            Name = evt.Name;
            Id = evt.AggregateId;
        }

        public void AdjustValue(decimal amount)
        {
            if (Id == Guid.Empty)
                throw new AggregateException("Not created!");

            RaiseEvent(new ValueChanged(amount));
        }

        protected void Apply(ValueChanged evt)
        {
            Value += evt.Amount;
        }
    }

    public class Created
    {
        public Guid AggregateId { get; protected set; }
        public string Name { get; protected set; }

        protected Created()
        {
        }

        public Created(Guid aggregateId, string name)
        {
            AggregateId = aggregateId;
            Name = name;
        }
    }

    public class ValueChanged
    {
        public decimal Amount { get; protected set; }

        protected ValueChanged()
        {
        }

        public ValueChanged(decimal amount)
        {
            Amount = amount;
        }
    }
}
